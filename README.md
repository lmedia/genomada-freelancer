# Genomada

Genomada is an open-source project manager app for freelancers. It is developed with Laravel 8+ using VueJS as front-end language. The code is available for all who wants to download and expand it.

## Features

Genomada has lots of features to help solo freelancers run their business and understand the economics of their daily work. See the full list of features here.

- Login an Register with email validation
- Dashboard and economics overview
- Work forecast and workload planner
- Project task management with Kanban boards and tasks list
- Project Overview with economic indicators
- Time register (integrated with Toggl Track)
- Proposals with link to share
- Analytics to understand your business (more profitable projects, desired price per hour vs. actual price per hour, etc)
- Weekly review, based on the GTD principles.

# Install

- Clone the repository
- Create a database in your development enviroment.
- Create you .env file according to the one provided as example (.env.example)
- Execute the migrations (php artisan migrate)
- Execute the database seeders (php artisan db:seed)
- Run the project (php artisan serve)

That’s it!

You will need to create an account first to be able to access Genomada. A verification email will be sent, so be sure to have defined the SMTP server in the .env file.

[**Buy me a coffee!**](https://www.buymeacoffee.com/lifetimemedi) (thanks!)

[![MadeWithLaravel.com shield](https://madewithlaravel.com/storage/repo-shields/3023-shield.svg)](https://madewithlaravel.com/p/genomada/shield-link)

Genomada is a personal side project of [Lifetime Media](https://lifetime-media.net/)
