<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveRecurrings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('recurrings');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('recurrings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('type')->default('income');
            $table->string('name');
            $table->text('desc')->nullable();
            $table->integer('periodicity')->default(12);
            $table->date('date_start');
            $table->date('date_end')->default('2099-12-31')->nullable();
            $table->double('amount');
            $table->timestamps();
        });
    }
}
