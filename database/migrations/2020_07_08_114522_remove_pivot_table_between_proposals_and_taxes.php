<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePivotTableBetweenProposalsAndTaxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('proposal_tax');
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('proposal_tax', function (Blueprint $table) {
            $table->id();
            $table->foreignId('proposal_id')->onDelete('cascade');
            $table->foreignId('tax_id')->onDelete('cascade');
            $table->integer('order');
            $table->timestamps();
        });
    }
}
