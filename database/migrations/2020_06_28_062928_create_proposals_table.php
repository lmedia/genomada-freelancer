<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('client_id');
            $table->foreignId('language_id');
            $table->string('slug');
            $table->string('name');
            $table->date('date');
            $table->string('reference')->nullable();
            $table->string('contact_person')->nullable();
            $table->longtext('desc')->nullable();
            $table->longtext('terms')->nullable();
            $table->timestamps();
        });

        Schema::create('proposal_concepts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('proposal_id')->onDelete('cascade');
            $table->string('name');
            $table->longtext('desc')->nullable();
            $table->double('price')->nullable();
            $table->date('finish_date')->nullable();
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('proposal_tax', function (Blueprint $table) {
            $table->id();
            $table->foreignId('proposal_id')->onDelete('cascade');
            $table->foreignId('tax_id')->onDelete('cascade');
            $table->integer('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals_concepts');
        Schema::dropIfExists('proposals_tax');
        Schema::dropIfExists('proposals');
    }
}
