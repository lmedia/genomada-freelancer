<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrepareFieldsForEncryption2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->text('name')->change();
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->text('name')->change();
            $table->text('contact')->change();
            $table->text('email')->change();
            $table->text('phone')->change();
            $table->text('vat_id')->change();
            $table->text('address')->change();
            $table->text('city')->change();
            $table->text('zip')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('name')->change();
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->string('name')->change();
            $table->string('contact')->change();
            $table->string('email')->change();
            $table->string('phone')->change();
            $table->string('vat_id')->change();
            $table->string('address')->change();
            $table->string('city')->change();
            $table->string('zip')->change();
        });
    }
}
