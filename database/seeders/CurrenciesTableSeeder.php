<?php

namespace Database\Seeders;

use App\Currency;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      // Empty the table
        Currency::truncate();

        DB::table('currencies')->insert(
        [
          'name' => 'Euro',
          'code' => 'EUR',
          'locale' => 'es-ES',
          'symbol' => '€',
          'format' => '1.0,00 €',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );

        DB::table('currencies')->insert(
        [
          'name' => 'US Dollar',
          'code' => 'USD',
          'locale' => 'en-US',
          'symbol' => '$',
          'format' => '$1,0.00',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );

        DB::table('currencies')->insert(
        [
          'name' => 'Pound Sterling',
          'locale' => 'en-GB',
          'code' => 'GBP',
          'symbol' => '£',
          'format' => '£1,0.00',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );

        DB::table('currencies')->insert(
        [
          'name' => 'Canadian Dollar',
          'code' => 'CAD',
          'locale' => 'fr-CA',
          'symbol' => '$',
          'format' => '$1,0.00',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );

        DB::table('currencies')->insert(
        [
          'name' => 'Australian Dollar',
          'code' => 'AUD',
          'locale' => 'en-AU',
          'symbol' => '$',
          'format' => '$1,0.00',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );

        DB::table('currencies')->insert(
        [
          'name' => 'Swedish Krona',
          'code' => 'SEK',
          'locale' => 'sv-SE',
          'symbol' => 'kr',
          'format' => '1 0,00 kr',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );

        DB::table('currencies')->insert(
        [
          'name' => 'Indian Rupee',
          'code' => 'INR',
          'locale' => 'hi-IN',
          'symbol' => '₹',
          'format' => '1,0.00₹',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );

        DB::table('currencies')->insert(
        [
          'name' => 'China Yuan Renminbi',
          'code' => 'CNY',
          'locale' => 'zh-CN',
          'symbol' => '¥',
          'format' => '¥1,0.00',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );

        DB::table('currencies')->insert(
        [
          'name' => 'Russian Ruble',
          'code' => 'RUB',
          'locale' => 'ru-RU',
          'symbol' => '₽',
          'format' => '1 0,00 ₽',
          'exchange_rate' => 1,
          'active' => 1,
        ]
      );
    }
}
