<?php

namespace Database\Seeders;

use App\Language;
use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        // Empty the table
        Language::truncate();

        Language::create(
          [
            'name' => 'English',
            'code' => 'en',
          ]
        );

        Language::create(
          [
            'name' => 'Español',
            'code' => 'es',
          ]
        );

        Language::create(
          [
            'name' => 'Català',
            'code' => 'ca',
          ]
        );
    }
}
