<?php

namespace Database\Seeders;

use Database\Seeders\CurrenciesTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Seed the currencies
        $this->call(CurrenciesTableSeeder::class);
    }
}
