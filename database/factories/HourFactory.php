<?php

namespace Database\Factories;

use App\Hour;
use App\Project;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class HourFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Hour::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $projects = Project::all()->random();
        $dia = rand(1, 31);
        $start = Carbon::create(2020, date('m'), rand(4, 28), 0, 0, 0);
        $end = Carbon::create(2020, date('m'), rand(4, 28), rand(1, 5), rand(10, 50), 0);
        $hours = round($end->diffInMinutes($start, true) / 60, 1);

        return [
            'project_id' => $projects->id,
            'user_id' => $projects->user_id,
            'name' => $this->faker->sentence(rand(3, 5)),
            'start' => $start->format('Y-m-d H:i:s'),
            'end' => $end->format('Y-m-d H:i:s'),
            'hours' => $hours,
            'billable' => rand(0, 1),
            'pending' => rand(0, 1),
        ];
    }
}
