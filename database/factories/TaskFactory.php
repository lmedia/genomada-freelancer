<?php

namespace Database\Factories;

use App\Project;
use App\Task;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $projects = Project::all()->random();

        return [
            'project_id' => $projects->id,
            'user_id' => $projects->user_id,
            'name' => $this->faker->sentence(rand(3, 5)),
            'desc' => $this->faker->sentence(rand(10, 50)),
            'focus' => rand(0, 1),
        ];
    }
}
