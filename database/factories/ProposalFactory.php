<?php

namespace Database\Factories;

use App\Client;
use App\Language;
use App\Proposal;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class ProposalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Proposal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $client = Client::all()->random();
        $language = Language::all()->random();

        return [
            'user_id' => $client->user_id,
            'client_id' => $client->id,
            'language_id' => $language->id,
            'name' => $this->faker->name,
            'date' => date('Y-m-d'),
            'reference' => 'PR-'.rand(50, 2000),
            'contact_person' => $this->faker->name,
            'desc' => $this->faker->paragraph(rand(2, 5), true),
            'terms' => $this->faker->paragraph(rand(10, 15), true),
        ];
    }
}
