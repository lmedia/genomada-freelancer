<?php

namespace Database\Factories;

use App\Mit;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class MitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::all()->random();

        return [
            'user_id' => $user->id,
            'day' => Carbon::now()->startOfWeek()->add(rand(0, 7), 'days')->format('Y-m-d'),
            'name' => $this->faker->sentence(rand(3, 5)),
            'order' => rand(0, 10),
        ];
    }
}
