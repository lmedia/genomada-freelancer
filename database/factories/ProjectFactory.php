<?php

namespace Database\Factories;

use App\Client;
use App\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $client = Client::all()->random();

        return [
            'client_id' => $client->id,
            'user_id' => $client->user_id,
            'name' => $this->faker->name,
            'desired_price_hour' => config('genomada.desired_price_hour'),
            'hours_estimated' => rand(20, 100),
            'type' => (rand(0, 1) == 1) ? 'fixed' : 'hours',
            'total_price' => rand(3000, 12000),
        ];
    }
}
