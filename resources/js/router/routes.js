function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', name: 'welcome', component: page('welcome.vue') },

  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },

  { path: '/projects', name: 'projects', component: page('projects/ProjectList.vue') },
  { path: '/projects/order-focus', name: 'projects.order', component: page('projects/ProjectOrderFocus.vue') },
  { path: '/project/add', name: 'project.add', component: page('projects/ProjectForm.vue') },
  { path: '/project/:id', name: 'project.home', component: page('projects/ProjectHome.vue') },
  { path: '/project/tasks/:id', name: 'project.tasks', component: page('projects/ProjectTasks.vue') },
  { path: '/project/kanban/:id', name: 'project.kanban', component: page('projects/ProjectKanban.vue') },
  { path: '/project/edit/:id', name: 'project.edit', component: page('projects/ProjectForm.vue') },
  { path: '/project/hours/:id', name: 'project.hours', component: page('projects/ProjectHours.vue') },
  { path: '/project/hours/toggl/:id', name: 'project.hours.toggl', component: page('projects/integrations/TogglHours.vue') },
  { path: '/project/economics/:id', name: 'project.economics', component: page('projects/ProjectEconomics.vue') },

  { path: '/clients', name: 'clients', component: page('clients/ClientsList.vue') },
  { path: '/client/add', name: 'client.add', component: page('clients/ClientForm.vue') },
  { path: '/client/:id', name: 'client.home', component: page('clients/ClientHome.vue') },
  { path: '/client/edit/:id', name: 'client.edit', component: page('clients/ClientForm.vue') },

  { path: '/hours', name: 'hours', component: page('hours/Hours.vue') },
  { path: '/hours/raw', name: 'hours.raw', component: page('hours/HoursRaw.vue') },
  { path: '/hours/toggl', name: 'hours.toggl', component: page('hours/integrations/TogglImport.vue') },

  { path: '/proposals', name: 'proposals', component: page('proposals/ProposalList.vue') },
  { path: '/proposal/add', name: 'proposal.add', component: page('proposals/ProposalForm.vue') },
  { path: '/proposal/edit/:id', name: 'proposal.edit', component: page('proposals/ProposalForm.vue') },

  { path: '/incomes/bill-hours', name: 'bill.hours', component: page('incomes/BillHours.vue') },

  {
    path: '/analytics',
    component: page('analytics/AnalyticsHome.vue'),
    children: [
      { path: '', redirect: { name: 'analytics.welcome' } },
      { path: 'home', name: 'analytics', component: page('analytics/AnalyticsWelcome.vue') },
      { path: 'bestprojects', name: 'analytics.bestprojects', component: page('analytics/BestProjects.vue') },
      { path: 'monthly-earnings', name: 'analytics.monthlyearnings', component: page('analytics/MonthlyEarnings.vue') },
    ]
  },

  {
    path: '/review',
    component: page('review/ReviewHome.vue'),
    children: [
      { path: '', redirect: { name: 'review' } },
      { path: 'home', name: 'review', component: page('review/ReviewWelcome.vue') },
      { path: 'projects', name: 'review.projects', component: page('review/ReviewProjects.vue') },
      { path: 'tasks', name: 'review.tasks', component: page('review/ReviewTasks.vue') },
    ]
  },


  { path: '/home', name: 'home', component: page('home.vue') },

  {
    path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'subscription', name: 'settings.subscription', component: page('settings/SubscriptionManagement.vue') },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') },
      { path: 'integrations', name: 'settings.integrations', component: page('settings/integrations.vue') },
      { path: 'account', name: 'settings.account', component: page('settings/account.vue') }
    ]
  },


  { path: '*', component: page('errors/404.vue') }
]
