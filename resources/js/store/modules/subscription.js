import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
  subscription: null,
}

// getters
export const getters = {
  subscription: state => state.subscription,
}

// mutations
export const mutations = {

  [types.FETCH_SUBSCRIPTION_SUCCESS](state, { subscription }) {
    state.subscription = subscription
  },

  [types.FETCH_SUBSCRIPTION_FAILURE](state) {
    state.subscription = null
  },

}

// actions
export const actions = {

  async fetchSubscription ({ commit }) {
    try {
      const { data } = await axios.get('/api/subscription')
      commit(types.FETCH_SUBSCRIPTION_SUCCESS, { subscription: data })
    } catch (e) {
      commit(types.FETCH_SUBSCRIPTION_FAILURE)
    }
  },

}
