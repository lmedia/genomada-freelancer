@php
$config = [
    'appName' => config('app.name'),
    'locale' => $locale = app()->getLocale(),
    'locales' => config('app.locales'),
    'githubAuth' => config('services.github.client_id'),
];
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>{{ config('app.name') }} / {{ $proposal->name }}</title>

  <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,200;0,400;1,200;1,400&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}">

</head>
<body>

    <div class="container">
      <div id="proposal">

        <div class="row mb-5">

          <div class="col-12 col-md-8">
            <h1>{{ $proposal->name }}</h1>
          </div>

          <div class="col-12 col-md-4 text-right">
            <p>
              <strong>{{ $proposal->reference }} · {{ $proposal->show_date }}</strong>
              <br/>{{ $proposal->client->name }}
              <br/>{{ $proposal->contact_person }}
            </p>
          </div>

        </div>

        <div class="row mb-5">
          <div class="col-12">
            {!! $proposal->desc !!}
          </div>
        </div>

        @foreach($proposal->concepts as $concept)
          <div class="row mb-5">
            <div class="col-10">
              <h3>{{ $concept->name }}</h3>
            </div>
            <div class="col-2 text-right">
              <h5 class="price">{{ currency($concept->price) }}</h5>
            </div>
          </div>
          <div class="row mb-5">
            <div class="col-12">
              {!! $concept->desc !!}
            </div>
          </div> 
        @endforeach   

        <div id="proposal-resume">

          <div class="row">
            <div class="col-8">
              <h4 class="mb-3">@lang('genomada.resume')</h4>
            </div>
            <div class="col-2 text-right">
              @lang('genomada.finish_date')
            </div>
            <div class="col-2 text-right">
              @lang('genomada.amount')
            </div>
          </div>

          @foreach($proposal->concepts as $concept)
            <div class="row slot-concept-proposal-resume">
              <div class="col-8">
                <p><strong>{{ $concept->name }}</strong></p>
              </div>
              <div class="col-2 text-right">
                <p>{{ $concept->show_finish_date }}</p>
              </div>
              <div class="col-2 text-right">
                <h5 class="price">{{ currency($concept->price) }}</h5>
              </div>
            </div>
          @endforeach   
        </div> 

        <div class="row mb-5" id="terms">
          <div class="col-12 text-sm">
            <p><strong>@lang('genomada.terms_and_conditions')</strong></p>
            {!! $proposal->terms !!}
          </div>
        </div>

      </div>
    </div>

    <div id="credits-genomada">

      <p>
        <a href="https://genomada.com" target="_blank"><img src="/img/genomada-logo.png"/></a>
      </p>
      <p>
        @lang('genomada.credits')
      </p>

    </div>

</body>
</html>
