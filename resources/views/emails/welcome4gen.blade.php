@component('mail::message')
{{-- Greeting --}}
# @lang('¡Hurra!')

{{-- Intro Lines --}}
<h2>¡Nuevo usuario en Genomada!</h2>
<p>{{$data['name']}} se ha dado de alta hoy con el email {{ $data['email']}}</p>.

@lang('Bien'),<br>
{{ config('app.name') }}

@endcomponent

