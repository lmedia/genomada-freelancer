@component('mail::message')
{{-- Greeting --}}
# @lang('¡Hola!')

{{-- Intro Lines --}}
<h2>¡Te damos la bienvenida a Genomada!</h2>
<p>Hola {{$data['name']}}, gracias por crear tu cuenta en <strong>Genomada</strong>.</p>
<p>Desde ahora en adelante podrás gestionar tu día a día como freelance a través del panel de control de la aplicación en <a href="https://go.genomada.com" target="_blank">https://go.genomada.com</a>.</p>
<p>El plan gratuito te permite crear hasta cinco proyectos y clientes. Para poder crear proyectos y clientes ilimitados, suscríbete a nuestro plan mensual o anual desde tu zona privada de usuario.</p>
<p>No dudes en contactarnos para cualquier duda que pueda surgirte.</p>
<p>¡Un saludo!</p>

{{-- Action Button --}}
@component('mail::button', ['url' => 'https://go.genomada.com', 'color' => 'success'])
Accede a Genomada
@endcomponent

@lang('Atentamente'),<br>
{{ config('app.name') }}

@endcomponent

