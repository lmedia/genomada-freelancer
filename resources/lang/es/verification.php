<?php

return [

    'verified' => '¡Tu email ha sido correctamente validado!',
    'invalid' => 'El link de validación no es correcto.',
    'already_verified' => 'El email ya ha sido verificado.',
    'user' => 'No podemos encontrar un usuario con este email.',
    'sent' => 'Te hemos enviado un correo electrónico con el enlace de validación.',

];
