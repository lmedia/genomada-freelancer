<?php

return [

  'monday' => 'Lunes',
  'tuesday' => 'Martes',
  'wednesday' => 'Miércoles',
  'thursday' => 'Jueves',
  'friday' => 'Viernes',
  'weekend' => 'Fin de semana',
  'this_week' => 'Esta semana',
  'last_week' => 'Semana pasada',
  'next_week' => 'Próxima semana',

  'terms_and_conditions' => 'Términos y condiciones',
  'resume' => 'Resumen',
  'finish_date' => 'Fecha prevista',
  'amount' => 'Importe',
  'credits' => 'Enviado con Genomada, la app de gestión de proyectos para freelancers.',
];
