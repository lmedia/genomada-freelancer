<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait Sluggable
{
    public static function bootSluggable()
    {
        static::saving(function ($model) {
            $settings = $model->sluggable();
            $field = $settings['source'];
            $model->slug = $model->generateSlug($model->{$field});
        });
    }

    abstract public function sluggable(): array;

    public function generateSlug($string)
    {
        return Str::slug($string, '-');
    }
}
