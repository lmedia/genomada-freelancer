<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    use Multitenantable;

    protected $fillable = [
        'name', 'percent', 'order',
    ];

    /**
     * The roles that belong to the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
