<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * Get the incomes.
     */
    public function clients()
    {
        return $this->hasMany(\App\Client::class);
    }
}
