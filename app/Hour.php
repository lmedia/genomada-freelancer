<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    use HasFactory;
    use Multitenantable;

    protected $fillable = [
        'name', 'project_id', 'start', 'end', 'billable', 'pending', 'hours', 'toggl_id',
    ];

    /**
     * Get the project.
     */
    public function project()
    {
        return $this->belongsTo(\App\Project::class)->withDefault();
    }

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }
}
