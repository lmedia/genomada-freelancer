<?php

namespace App;

use App\Traits\Multitenantable;
use App\Traits\EncryptableDbAttribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Client extends Model
{
    use HasFactory;
    use Multitenantable;
    use EncryptableDbAttribute;

    /** @var array The attributes that should be encrypted/decrypted */
    protected $encryptable = [
        'name',
        'contact',
        'email',
        'phone',
        'vat_id',
        'address',
        'city',
        'zip',
    ];

    protected $fillable = [
        'name',
        'toggl_id',
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }

    /**
     * Get the projects.
     */
    public function projects()
    {
        return $this->hasMany(\App\Project::class);
    }

    /**
     * Get the country.
     */
    public function country()
    {
        return $this->belongsTo(\App\Country::class)->withDefault();
    }

    /**
     * Get the incomes.
     */
    public function incomes()
    {
        return $this->hasMany(\App\Income::class);
    }

    /**
     * Get the outcomes.
     */
    public function outcomes()
    {
        return $this->hasMany(\App\Outcome::class);
    }

    /**
     * Get the hours.
     */
    public function hours()
    {
        return $this->hasMany(\App\Hour::class);
    }

    /*
    Attributes
    */
    public function getTotalAttribute()
    {
        $total = 0;
        foreach ($this->projects()->get() as $project) {
            $total += $project->benefit;
        }

        return $total;
    }

    public function getHoursAttribute()
    {
        $total = 0;
        foreach ($this->projects()->get() as $project) {
            $total += $project->hours;
        }

        return $total;
    }

    public function getPriceHourAttribute()
    {
        $price_hour = 0;
        $projects = 0;
        foreach ($this->projects()->get() as $project) {
            $price_hour += $project->price_hour;
            $projects++;
        }
        if ($price_hour > 0) {
            $price_hour = round($price_hour / $projects, 2);
        }
        $user = Auth::user();
        $price_hour = currency_format((string) $price_hour, $user->currency->code);

        return $price_hour;
    }
}
