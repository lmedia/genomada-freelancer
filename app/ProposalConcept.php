<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProposalConcept extends Model
{
    protected $fillable = [
        'name', 'desc', 'price', 'finish_date', 'order',
    ];

    /**
     * The proposal.
     */
    public function proposal()
    {
        return $this->belongsTo(\App\Proposal::class);
    }

    public function getShowFinishDateAttribute()
    {
        return Carbon::parse($this->finish_date)->format('d/m/Y');
    }
}
