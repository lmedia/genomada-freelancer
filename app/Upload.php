<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    use Multitenantable;

    protected $fillable = [
        'order', 'name', 'filename', 'task_id',
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }

    /**
     * Get the task.
     */
    public function task()
    {
        return $this->belongsTo(\App\Task::class)->withDefault();
    }
}
