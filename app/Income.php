<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $fillable = [
        'name', 'date', 'amount', 'project_id', 'billed_from_timer',
    ];

    /**
     * Get the project.
     */
    public function project()
    {
        return $this->belongsTo(\App\Project::class)->withDefault();
    }
}
