<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * The roles that belong to the user.
     */
    public function proposals()
    {
        return $this->belongsToMany('App\Role');
    }
}
