<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    use Multitenantable;

    protected $fillable = [
        'name',
        'order',
        'project_id',
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }

    /**
     * Get the projects.
     */
    public function projec()
    {
        return $this->belongsTo(\App\Project::class)->withDefault();
    }

    /**
     * Get the tasks.
     */
    public function tasks()
    {
        return $this->hasMany(\App\Task::class);
    }
}
