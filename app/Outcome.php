<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outcome extends Model
{
    protected $fillable = [
        'name', 'date', 'amount', 'project_id',
    ];

    /**
     * Get the project.
     */
    public function project()
    {
        return $this->belongsTo(\App\Project::class)->withDefault();
    }
}
