<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class IntegrationToggl extends Model
{
    protected $table = 'integration_toggl';

    use Multitenantable;

    protected $fillable = [
        'user_id', 'projects',
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
