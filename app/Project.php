<?php

namespace App;

use App\Traits\Multitenantable;
use App\Traits\EncryptableDbAttribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Project extends Model
{
    use HasFactory;
    use Multitenantable;
    use Mediable;
    use EncryptableDbAttribute;

    /** @var array The attributes that should be encrypted/decrypted */
    protected $encryptable = [
        'name',
    ];

    protected $fillable = [
        'name', 'client_id', 'focus', 'hours_estimated', 'type', 'desired_price_hour', 'total_price',
        'order', 'archived',
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }

    /**
     * Get the client.
     */
    public function client()
    {
        return $this->belongsTo(\App\Client::class)->withDefault();
    }

    /**
     * Get the tasks.
     */
    public function tasks()
    {
        return $this->hasMany(\App\Task::class);
    }

    /**
     * Get the workloads.
     */
    public function workloads()
    {
        return $this->hasMany(\App\Workload::class);
    }

    /**
     * Get the boards.
     */
    public function boards()
    {
        return $this->hasMany(\App\Board::class);
    }

    /**
     * Get the hours.
     */
    public function hours()
    {
        return $this->hasMany(\App\Hour::class);
    }

    /**
     * Get the incomes.
     */
    public function incomes()
    {
        return $this->hasMany(\App\Income::class);
    }

    /**
     * Get the expenses.
     */
    public function outcomes()
    {
        return $this->hasMany(\App\Outcome::class);
    }

    /*
    Attributes
    */
    public function getTotalAttribute()
    {
        if ($this->type == 'free') {
            return 0;
        }
        if ($this->type == 'hours') {
            $total = $this->hours * $this->desired_price_hour;
            return round($total, 2);
        }

        return $this->total_price;
    }

    public function getIncomesAttribute()
    {
        return round($this->incomes()->sum('amount'), 2);
    }

    public function getOutcomesAttribute()
    {
        return round($this->outcomes()->sum('amount'), 2);
    }

    public function getBenefitAttribute()
    {
        if ($this->type == 'fixed') {
            return round($this->total - $this->outcomes, 2);
        } else {
            return round(($this->incomes + $this->estimated_earnings_pending) - $this->outcomes, 2);
        }
    }

    public function getBalanceAttribute()
    {
        return round($this->incomes - $this->outcomes, 2);
    }

    public function getHoursAttribute()
    {
        return round($this->hours()->sum('hours'), 2);
    }

    public function getHoursPendingAttribute()
    {
        return round($this->hours()->where('pending', '1')->sum('hours'), 2);
    }

    public function getHoursConfirmedAttribute()
    {
        return round($this->hours()->where('pending', '0')->sum('hours'), 2);
    }

    public function getEstimatedEarningsPendingAttribute()
    {
        if ($this->type == 'hours') {
            return round($this->hours()->where('pending', '1')->where('billable', '1')->sum('hours') * $this->desired_price_hour, 2);
        } elseif ($this->type == 'fixed') {
            return round($this->total - $this->incomes, 2);
        }

        return 0;
    }

    public function getPriceHourAttribute()
    {
        if ($this->finished == 1) {
            if ($this->hours > 0) {
                $price_hour = round(($this->benefit / $this->hours), 2);

                return $price_hour;
            } else {
                return 0;
            }
        }

        if ($this->type == 'free') {
            return 0;
        }

        if ($this->type == 'hours') {
            if ($this->hours > 0) {
                $price_hour = round(($this->benefit / $this->hours), 2);

                return $price_hour;
            } else {
                return 0;
            }
        }

        if ($this->type == 'fixed') {
            $go = false;
            if ($this->hours > ($this->hours_estimated / 4)) {
                $go = true;
            }
            if ($this->finished == 1) {
                $go = true;
            }
            if ($go) {
                $price_hour = round(($this->benefit / $this->hours), 2);

                return $price_hour;
            } else {
                return 0;
            }
        }

        return 0;
    }
}
