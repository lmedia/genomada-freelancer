<?php

namespace App\Jobs;

use App\Hour;
use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Integrations\Toggl\TogglApi;

class SyncToggl implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;

        if ($user) {
            if ($user->token_toggl) {
                $toggl = new TogglApi($user->token_toggl);
                $time_entries = $toggl->getTimeEntries();
                $time_entries = json_decode(json_encode($time_entries), true);

                if (count($time_entries) > 0) {
                    foreach ($time_entries as $item) {
                        if (isset($item['pid'])) {
                            $proj = Project::where('toggl_id', $item['pid'])->first(['id', 'type']);
                            if ($proj) {
                                $billable = 0;
                                if ($proj->type == 'hours') {
                                    $billable = 1;
                                }

                                // Check if that hour ID was already added. If so, then update
                                if ($item['id'] > 0) {
                                    $hour = Hour::where('toggl_id', $item['id'])->first();
                                    if (! $hour) {
                                        $hour = new Hour();
                                    }
                                } else {
                                    $hour = new Hour();
                                }

                                // Define hours
                                $start = Carbon::parse($item['start']);
                                $end = Carbon::parse($item['stop']);
                                $hours = round($end->diffInMinutes($start, true) / 60, 1);

                                // Add the fields
                                $hour->user_id = $user->id;
                                $hour->name = (trim($item['description']) == '') ? '-' : trim($item['description']);
                                $hour->project_id = $proj->id;
                                $hour->toggl_id = $item['id'];
                                $hour->start = $start->format('Y-m-d H:i:s');
                                $hour->end = $end->format('Y-m-d H:i:s');
                                $hour->hours = $hours;
                                $hour->billable = $billable;
                                $hour->save();
                            }
                        }
                    }

                    return true;
                }
            }
        }
    }
}
