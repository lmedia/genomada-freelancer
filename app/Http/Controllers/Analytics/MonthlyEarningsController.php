<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\Income;
use App\Outcome;
use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class MonthlyEarningsController extends Controller
{
    /**
     * Gets the best projects for ApexCharts.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $array = [];
        $user = Auth::user();

        $date = Carbon::now()->subMonth(6)->firstOfMonth();
        $today = Carbon::now()->firstOfMonth();

        $balance = [];
        while ($date->firstOfMonth() < $today) {
            $date->addMonth(1);

            $incomes = 0;
            $expenses = 0;
            foreach (Project::with('incomes')->with('outcomes')->get() as $project) {
                $start = $date->firstOfMonth()->format('Y-m-d');
                $end = $date->lastOfMonth()->format('Y-m-d');
                foreach ($project->incomes()->whereBetween('date', [$start, $end])->get() as $item) {
                    $incomes += $item->amount;
                }
                foreach ($project->outcomes()->whereBetween('date', [$start, $end])->get() as $item) {
                    $expenses += $item->amount;
                }
            }

            array_push($balance, [
                'date' => $date->firstOfMonth()->format('M Y'),
                'balance' => ($incomes - $expenses),
                'balance_formatted' => currency_format(($incomes - $expenses), $user->currency->code),
                'incomes' => $incomes,
                'expenses' => $expenses,
            ]);
        }

        // Prepare array of data
        $balances = [];
        $balances_formatted = [];
        $months = [];
        $boxes = [];

        foreach ($balance as $item) {
            array_push($balances, $item['balance']);
            array_push($balances_formatted, $item['balance_formatted']);
            array_push($months, $item['date']);
            array_push($boxes, $item);
        }

        // Prepare to return data
        $collection = [];
        array_push($collection, [
            'balances' => $balances,
            'balances_formatted' => $balances_formatted,
            'months' => $months,
            'boxes' => $boxes,
        ]);

        return ResponseBuilder::success($collection);
    }
}
