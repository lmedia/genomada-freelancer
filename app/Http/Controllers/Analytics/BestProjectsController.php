<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class BestProjectsController extends Controller
{
    /**
     * Gets the best projects for ApexCharts.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $array = [];
        $projects = Project::where('type', '!=', 'free')->orderBy('desired_price_hour', 'desc')->get();
        if ($projects->count() == 0) {
            return ResponseBuilder::success(null);
        }

        foreach ($projects as $item) {
            array_push($array, [
                'name' => $item->name,
                'client' => $item->client->name,
                'price_hour' => $item->price_hour,
            ]);
        }

        $sortArray = [];

        foreach ($array as $item) {
            foreach ($item as $key => $value) {
                if (! isset($sortArray[$key])) {
                    $sortArray[$key] = [];
                }
                $sortArray[$key][] = $value;
            }
        }
        $orderby = 'price_hour';
        array_multisort($sortArray[$orderby], SORT_DESC, $array);

        $limit = 10;
        $user = Auth::user();

        // Prepare array of data
        $counter = 0;
        $price_hour = [];
        $price_hour_formatted = [];
        $clients = [];
        foreach ($array as $item) {
            if ($counter < $limit) {
                array_push($price_hour, $item['price_hour']);
                array_push($price_hour_formatted, currency_format($item['price_hour'], $user->currency->code));
                array_push($clients, $item['name']);
                $counter++;
            }
        }

        // Prepare to return data
        $collection = [];
        array_push($collection, [
            'price_hour' => $price_hour,
            'clients' => $clients,
            'price_hour_formatted' => $price_hour_formatted,
        ]);

        return ResponseBuilder::success($collection);
    }
}
