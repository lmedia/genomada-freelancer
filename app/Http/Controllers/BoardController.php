<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\Board;
use App\Project;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = Project::find($request->projectId);
        if (! $project) {
            return ResponseBuilder::error(250);
        }

        $request->validate([
            'name' => 'required|string',
        ]);

        // We put it the last
        $order = 99;

        // Phase itself
        $item = new Board([
            'user_id' => $project->user_id,
            'project_id' => $project->id,
            'name' => $request->name,
            'order' => $order,
        ]);
        $item->save();

        return ResponseBuilder::success();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function show(Board $board)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function edit(Board $board)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Board $board)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function destroy(Board $board)
    {
        $board->delete();

        return ResponseBuilder::success();
    }

    /**
     * Gets the boards list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchBoards(Request $request)
    {
        $collection = [];
        foreach (Board::where('project_id', $request->projectId)->orderBy('order', 'asc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Saves the KanBan status and orders.
     *
     * @return array[]
     */
    public function saveKanBan(Request $request)
    {
        $orderPanel = 1;
        foreach ($request->children as $panel) {
            $item = Board::find($panel['id']);
            $item->order = $orderPanel;
            $item->save();
            $orderPanel++;

            $orderTasks = 1;
            foreach ($panel['children'] as $task) {
                $item2 = Task::find($task['id']);
                $item2->board_id = $item->id;
                $item2->order = $orderTasks;
                $item2->save();
                $orderTasks++;
            }
        }

        return ResponseBuilder::success();
    }

    /**
     * Rename a panel.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function renamePanel(Request $request)
    {
        $item = Board::find($request->id);
        if ($item) {
            $item->name = $request->name;
            $item->save();

            return ResponseBuilder::success();
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Returns the task for the KanBan popup.
     *
     * @return array[]
     */
    public function loadTaskForKanBan(Request $request)
    {
        $task = Task::find($request->id);
        if (! $task) {
            ResponseBuilder::error(250);
        }

        return ResponseBuilder::success($task);
    }

    /**
     * Fetches the projects.
     *
     * @return Response
     */
    public function loadKanban(Request $request)
    {
        $project = Project::where('id', $request->projectId)
            ->with(['boards' => function ($query) {
                $query->orderBy('order', 'asc');
                $query->with(['tasks' => function ($query) {
                    $query->orderBy('order', 'asc');
                }]);
            }])
            ->get();
        if (!$project) {
            ResponseBuilder::error(250);
        }

        return ResponseBuilder::success($project);
    }

    /**
     * Creates task from KanBan.
     *
     * @return Response
     */
    public function createTaskFromKanBan(Request $request)
    {
        $panel = Board::find($request->pId);
        if (! $panel) {
            return ResponseBuilder::error(250);
        }
        $project = Project::find($panel->project_id);
        if (! $project) {
            return ResponseBuilder::error(250);
        }

        $request->validate([
            'name' => 'required|string',
        ]);

        // We put it the last
        $order = 99;
        $due_date = null;

        // Task itself
        $item = new Task([
            'project_id' => $project->id,
            'board_id' => $panel->id,
            'name' => $request->get('name'),
            'due_date' => $due_date,
            'order' => $order,
        ]);

        $item->save();

        return ResponseBuilder::success();
    }
}
