<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\Board;
use App\Hour;
use App\Income;
use App\Outcome;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use MediaUploader;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'client_id' => 'required|exists:clients,id',
            'hours_estimated' => 'required|integer',
        ]);

        $type = $request->get('type');
        $hours_estimated = $request->get('hours_estimated');
        if ($type == 'hours') {
            $desired_price_hour = $request->get('desired_price_hour');
            $hours_estimated = 0;
            $total_price = 0;
        } else {
            $desired_price_hour = 0;
            $total_price = $request->get('total_price');
            if ($total_price > 0) {
                $desired_price_hour = $total_price / $hours_estimated;
            }
        }

        $item = new Project([
            'name' => trim($request->get('name')),
            'client_id' => $request->get('client_id'),
            'type' => $type,
            'hours_estimated' => $hours_estimated,
            'total_price' => $total_price,
            'desired_price_hour' => round($desired_price_hour, 2),
        ]);
        $item->save();

        return ResponseBuilder::success($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $item = Project::find($project->id);
        if ($item) {
            $user = Auth::user();
            $collection = [
                'id' => $item->id,
                'name' => $item->name,
                'client' => $item->client->name,
                'total' => currency_format((string) $item->total, $user->currency->code),
                'balance' => currency_format((string) $item->balance, $user->currency->code),
                'benefit' => currency_format((string) $item->benefit, $user->currency->code),
                'hours_estimated' => $item->hours_estimated.'h',
                'hours' => $item->hours.'h',
                'desired_price_hour' => currency_format((string) $item->desired_price_hour, $user->currency->code),
                'price_hour' => currency_format((string) $item->price_hour, $user->currency->code),
                'incomes' => currency_format((string) $item->incomes, $user->currency->code),
                'outcomes' => currency_format((string) $item->outcomes, $user->currency->code),
                'hours_pending' => $item->hours_pending.'h',
                'hours_confirmed' => $item->hours_confirmed.'h',
                'estimated_earnings_pending' => currency_format((string) $item->estimated_earnings_pending, $user->currency->code),
                'tasks_count' => $item->tasks()->count(),
                'focus' => $item->focus,
                'order' => $item->order,
                'archived' => $item->archived,
                'finished' => $item->finished,
                'type' => $item->type,
                'toggl_id' => $item->toggl_id,
            ];
        } else {
            $collection = new Project();
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Returns a raw project or a blank one.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function fetchRaw(Request $request)
    {
        $item = Project::find($request->id);
        if (! $item) {
            $item = new Project();
        }

        return ResponseBuilder::success($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'name' => 'required',
            'client_id' => 'required|exists:clients,id',
            'hours_estimated' => 'required|integer',
        ]);

        $item = Project::find($request->get('id'));
        if ($item) {
            $type = $request->get('type');
            $hours_estimated = $request->get('hours_estimated');
            if ($type == 'hours') {
                $desired_price_hour = $request->get('desired_price_hour');
                $hours_estimated = 0;
                $total_price = 0;
            } else {
                $desired_price_hour = 0;
                $total_price = $request->get('total_price');
                if ($total_price > 0) {
                    $desired_price_hour = $total_price / $hours_estimated;
                }
            }

            $item->name = trim($request->get('name'));
            $item->client_id = $request->get('client_id');
            $item->type = $type;
            $item->hours_estimated = $hours_estimated;
            $item->total_price = $total_price;
            $item->desired_price_hour = round($desired_price_hour, 2);
            $item->save();

            return ResponseBuilder::success($item);
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $item = Project::find($project->id);
        if (! $item) {
            return ResponseBuilder::error(250);
        }

        // Delete the attachments to free up space
        foreach ($item->tasks()->get() as $task) {

            // Delete the attachments to free up space
            $directory = null;
            foreach ($task->getMedia('taskfile') as $media) {
                $directory = $media->directory;
                $filename = $media->directory.'/'.$media->filename.'.'.$media->extension;
                if (Storage::disk('local')->exists($filename)) {
                    Storage::disk('local')->delete($filename);
                }
                $media->forceDelete();
            }
            if ($directory) {
                Storage::disk('local')->deleteDirectory($directory);
            }

            // Delete the task
            $task->forceDelete();
        }

        // Remove other things
        Board::where('project_id', $item->id)->delete();
        Hour::where('project_id', $item->id)->delete();

        // Update all incomes that have this project_id to have null
        Income::where('project_id', $item->id)->update(['project_id' => null]);
        Outcome::where('project_id', $item->id)->update(['project_id' => null]);

        $item->delete();

        return ResponseBuilder::success();
    }


    /**
     * Gets the counters list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function projectsCounters()
    {
        $collection = [
            'active' => Project::where('finished', 0)->where('archived', 0)->where('focus', 1)->count(),
            'paused' => Project::where('finished', 0)->where('archived', 0)->where('focus', 0)->count(),
            'finished' => Project::where('finished', 1)->count(),
            'archived' => Project::where('archived', 1)->count(),
        ];

        return ResponseBuilder::success($collection);
    }


    /**
     * Gets the projects list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetch()
    {
        $collection = [];
        $user = Auth::user();
        $query = Project::orderBy('order', 'asc')->orderBy('name', 'asc')->get();
        foreach ($query as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'client' => $item->client->name,
                'total' => currency_format((string) $item->total, $user->currency->code),
                'benefit' => currency_format((string) $item->benefit, $user->currency->code),
                'balance' => currency_format((string) $item->balance, $user->currency->code),
                'incomes' => currency_format((string) $item->incomes, $user->currency->code),
                'outcomes' => currency_format((string) $item->outcomes, $user->currency->code),
                'overworked' => $item->hours > $item->hours_estimated ? 1 : 0,
                'hours_estimated' => ($item->hours_estimated > 0) ? $item->hours_estimated.'h' : '',
                'hours' => $item->hours.'h',
                'hours_pending' => $item->hours_pending.'h',
                'hours_confirmed' => $item->hours_confirmed.'h',
                'estimated_earnings_pending' => currency_format((string) $item->estimated_earnings_pending, $user->currency->code),
                'desired_price_hour' => currency_format((string) $item->desired_price_hour, $user->currency->code),
                'price_hour' => ($item->price_hour > 0) ? currency_format((string) $item->price_hour, $user->currency->code) : '-',
                'focus' => $item->focus,
                'order' => $item->order,
                'archived' => $item->archived,
                'finished' => $item->finished,
                'type' => $item->type,
                'tasks_count' => $item->tasks()->count(),
                'toggl_id' => $item->toggl_id,
            ]);
        }

        return ResponseBuilder::success($collection);
    }


    /**
     * Gets the projects list (active).
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchActive()
    {
        $collection = [];
        $user = Auth::user();
        $query = Project::where('finished', 0)->where('archived', 0)->orderBy('order', 'asc')->orderBy('name', 'asc')->get();
        foreach ($query as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'client' => $item->client->name,
                'total' => currency_format((string) $item->total, $user->currency->code),
                'benefit' => currency_format((string) $item->benefit, $user->currency->code),
                'balance' => currency_format((string) $item->balance, $user->currency->code),
                'incomes' => currency_format((string) $item->incomes, $user->currency->code),
                'outcomes' => currency_format((string) $item->outcomes, $user->currency->code),
                'hours_estimated' => ($item->hours_estimated > 0) ? $item->hours_estimated . 'h' : '',
                'hours' => $item->hours . 'h',
                'hours_pending' => $item->hours_pending . 'h',
                'hours_confirmed' => $item->hours_confirmed . 'h',
                'estimated_earnings_pending' => currency_format((string) $item->estimated_earnings_pending, $user->currency->code),
                'desired_price_hour' => currency_format((string) $item->desired_price_hour, $user->currency->code),
                'price_hour' => ($item->price_hour > 0) ? currency_format((string) $item->price_hour, $user->currency->code) : '-',
                'focus' => $item->focus,
                'order' => $item->order,
                'archived' => $item->archived,
                'finished' => $item->finished,
                'type' => $item->type,
                'tasks_count' => $item->tasks()->count(),
                'toggl_id' => $item->toggl_id,
            ]);
        }

        return ResponseBuilder::success($collection);
    }
    /**
     * Gets the projects hours.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchHours(Request $request)
    {
        $collection = [];
        foreach (Hour::where('project_id', $request->id)->orderBy('end', 'desc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'start' => $item->start,
                'end' => $item->end,
                'hours' => $item->hours,
                'hours_pending' => $item->hours_pending,
                'hours_confirmed' => $item->hours_confirmed,
                'billable' => $item->billable,
                'pending' => $item->pending,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Gets the project incomes.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchIncomes(Request $request)
    {
        $collection = [];
        foreach (Income::where('project_id', $request->id)->orderBy('date', 'desc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'amount' => $item->amount,
                'show_amount' => currency_format((string) $item->amount),
                'date' => $item->date,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Gets the project outcomes.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchOutcomes(Request $request)
    {
        $collection = [];
        foreach (Outcome::where('project_id', $request->id)->orderBy('date', 'desc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'amount' => $item->amount,
                'show_amount' => currency_format((string) $item->amount),
                'date' => $item->date,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Toggles the focus state of the project.
     */
    public function toggleFocus(Request $request)
    {
        $item = Project::find($request->id);
        if ($item) {
            if ($item->focus == 0) {
                $item->focus = 1;
            } else {
                $item->focus = 0;
            }
            $item->save();

            return ResponseBuilder::success();
        }

        return ResponseBuilder::error(ApiCode::SOMETHING_WENT_WRONG);
    }

    /**
     * Toggles the archived state of the project.
     */
    public function toggleArchive(Request $request)
    {
        $item = Project::find($request->id);
        if ($item) {
            if ($item->archived == 0) {
                $item->archived = 1;
            } else {
                $item->archived = 0;
            }
            $item->save();

            return ResponseBuilder::success();
        }

        return ResponseBuilder::error(ApiCode::SOMETHING_WENT_WRONG);
    }

    /**
     * Toggles the finished state of the project.
     */
    public function toggleFinish(Request $request)
    {
        $item = Project::find($request->id);
        if ($item) {
            if ($item->finished == 0) {
                $item->finished = 1;
            } else {
                $item->finished = 0;
            }
            $item->save();

            return ResponseBuilder::success();
        }

        return ResponseBuilder::error(ApiCode::SOMETHING_WENT_WRONG);
    }

    /**
     * Orders the projects with the array given.
     */
    public function order(Request $request)
    {
        $order = 0;
        $data = $request->all();
        foreach ($data as $key => $value) {
            $item = Project::find($value[0]['id']);
            if (! $item) {
                return response()->json(false, 422);
            }
            $item->order = $order;
            $item->save();
            $order++;
        }

        return ResponseBuilder::success();
    }

    /**
     * Sets the image.
     */
    public function setImage(Request $request)
    {
        $project = Project::find($request->id);
        if (! $project) {
            return ResponseBuilder::error(250);
        }

        $ext = substr(strrchr($request->image['info']['name'], '.'), 1);
        $filename = 'trashable/'.$project->id.'.'.$ext;

        $img = $request->image['dataUrl'];
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
        $file = Storage::disk('local')->put($filename, $data);

        $path_to_file = str_replace('/public', '/storage/app', $_SERVER['DOCUMENT_ROOT']).'/'.$filename;

        $media = MediaUploader::fromSource($path_to_file)
            ->toDestination('local', 'projects/'.Auth::id())
            ->upload();
        foreach ($project->getMedia('image') as $imagen) {
            $imagen->delete();
        }
        $project->attachMedia($media, ['image']);

        Storage::disk('local')->delete($filename);

        return ResponseBuilder::success();
    }

    /**
     * Gets the thumbnail.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getThumbnail(Request $request)
    {
        $task = Project::find($request->id);
        if ($task) {
            $th = $task->getMedia('image')->first();
            if ($th) {
                $filename = $th->directory.'/'.$th->filename.'.'.$th->extension;
                if (Storage::disk('local')->exists($filename)) {
                    return response(Storage::get($filename, 200))->header('Content-Type', $th->mime_type);
                }
            }
        }

        return response(Storage::disk('local')->get('placeholder.png'), 200)->header('Content-Type', 'image/png');
    }
}
