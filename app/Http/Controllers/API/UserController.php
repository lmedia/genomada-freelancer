<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Creates an intent for payment so we can capture the payment
     * method for the user.
     *
     * @param Request $request The request data from the user.
     */
    public function getSetupIntent(Request $request)
    {
        return $request->user()->createSetupIntent();
    }

    /**
     * Gets current user subscription.
     *
     * @param Request $request The request data from the user.
     */
    public function getStatus(Request $request)
    {
        return $request->user()->subscribed('Genomada');
    }

    /**
     * Updates a subscription for the user.
     *
     * @param Request $request The request containing subscription update info.
     */
    public function updateSubscription(Request $request)
    {
        $user = $request->user();
        $planID = $request->get('plan');
        $paymentID = $request->get('payment');

        // Check user's plane
        if (! $user->subscribed('Genomada')) {
            $user->newSubscription('Genomada', $planID)->create($paymentID);
        } else {
            $user->subscription('Genomada')->swap($planID);
        }
        $user->save();

        return response()->json([
            'subscription_updated' => true,
        ]);
    }

    /**
     * Adds a payment method to the current user.
     *
     * @param Request $request The request data from the user.
     */
    public function postPaymentMethods(Request $request)
    {
        $user = $request->user();
        $paymentMethodID = $request->get('payment_method');

        if ($user->stripe_id == null) {
            $user->createAsStripeCustomer();
        }

        $user->addPaymentMethod($paymentMethodID);
        $user->updateDefaultPaymentMethod($paymentMethodID);

        return response()->json(null, 204);
    }

    /**
     * Returns the payment methods the user has saved.
     *
     * @param Request $request The request data from the user.
     */
    public function getPaymentMethods(Request $request)
    {
        $user = $request->user();

        $methods = [];

        if ($user->hasPaymentMethod()) {
            foreach ($user->paymentMethods() as $method) {
                array_push($methods, [
                    'id' => $method->id,
                    'brand' => $method->card->brand,
                    'last_four' => $method->card->last4,
                    'exp_month' => $method->card->exp_month,
                    'exp_year' => $method->card->exp_year,
                ]);
            }
        }

        return response()->json($methods);
    }

    /**
     * Removes a payment method for the current user.
     *
     * @param Request $request The request data from the user.
     */
    public function removePaymentMethod(Request $request)
    {
        $user = $request->user();
        $paymentMethodID = $request->get('id');

        $paymentMethods = $user->paymentMethods();

        foreach ($paymentMethods as $method) {
            if ($method->id == $paymentMethodID) {
                $method->delete();
                break;
            }
        }

        return response()->json(null, 204);
    }

    /**
     * Returns the status of the user.
     *
     * @param Request $request The request data from the user.
     */
    public function cancelSubscription(Request $request)
    {
        $user = $request->user();
        if ($user->subscribed('Genomada')) {
            $user->subscription('Genomada')->cancelNow();
        }

        return response()->json([
            'subscription_cancelled' => true,
        ]);
    }
}
