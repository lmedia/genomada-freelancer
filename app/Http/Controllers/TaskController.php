<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use Stevebauman\Purify\Facades\Purify;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $lastTask = Task::where('project_id', $request->get('project_id'))->orderBy('order', 'asc')->first(['order']);
        if (! $lastTask) {
            $order = 0;
        } else {
            $order = $lastTask->order - 1;
        }

        $item = new Task([
            'name' => trim($request->get('name')),
            'project_id' => $request->get('project_id'),
            'order' => $order,
        ]);
        $item->save();

        return ResponseBuilder::success($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        $item = Task::find($task->id);

        $collection = [
            'id' => $item->id,
            'name' => $item->name,
            'desc' => $item->desc,
            'focus' => $item->focus,
            'completed' => $item->completed,
            'due_date' => $item->due_date,
            'show_due_date' => $item->show_due_date,
            'show_formatted_due_date' => $item->show_formatted_due_date,
            'inbox' => $item->inbox,
            'order' => $item->order,
            'uploads' => $item->getMedia('taskfile'),
        ];

        return ResponseBuilder::success($collection);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $item = Task::find($request->get('id'));
        if ($item) {
            $item->name = trim($request->get('name'));
            $item->desc = Purify::clean($request->get('desc'));
            if ($request->get('due_date') != null) {
                $item->due_date = Carbon::parse($request->get('due_date'))->addDays(1)->format('Y-m-d');
            }
            $item->save();

            return ResponseBuilder::success($item);
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task = Task::find($task->id);
        if (! $task) {
            return ResponseBuilder::error(250);
        }

        // Delete the attachments to free up space
        $directory = null;
        foreach ($task->getMedia('taskfile') as $media) {
            $directory = $media->directory;
            $filename = $media->directory.'/'.$media->filename.'.'.$media->extension;
            if (Storage::disk('local')->exists($filename)) {
                Storage::disk('local')->delete($filename);
            }
            $media->forceDelete();
        }
        if ($directory) {
            Storage::disk('local')->deleteDirectory($directory);
        }

        $task->forceDelete();

        return ResponseBuilder::success();
    }

    /**
     * Gets the projects list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetch(Request $request)
    {
        $collection = [];
        foreach (Task::where('project_id', $request->projectId)->orderBy('order', 'asc')->orderBy('name', 'asc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'desc' => $item->desc,
                'focus' => $item->focus,
                'completed' => $item->completed,
                'due_date' => $item->due_date,
                'show_formatted_due_date' => $item->show_formatted_due_date,
                'inbox' => $item->inbox,
                'order' => $item->order,
                'board_id' => $item->board_id,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Toggles the focus state of the task.
     */
    public function toggleFocus(Request $request)
    {
        $item = Task::find($request->id);
        if ($item) {
            if ($item->focus == 0) {
                $item->focus = 1;
            } else {
                $item->focus = 0;
            }
            $item->save();

            return ResponseBuilder::success();
        }

        return ResponseBuilder::error(ApiCode::SOMETHING_WENT_WRONG);
    }

    /**
     * Toggles the completed state of the task.
     */
    public function toggleCompleted(Request $request)
    {
        $item = Task::find($request->id);
        if ($item) {
            if ($item->completed == 0) {
                $item->completed = 1;
            } else {
                $item->completed = 0;
            }
            $item->save();

            return ResponseBuilder::success();
        }

        return ResponseBuilder::error(ApiCode::SOMETHING_WENT_WRONG);
    }

    /**
     * Stores board.
     */
    public function storeBoard(Request $request)
    {
        $item = Task::find($request->data['id']);
        if ($item) {
            $item->board_id = $request->data['board_id'];
            $item->save();

            return ResponseBuilder::success();
        }

        return ResponseBuilder::error(ApiCode::SOMETHING_WENT_WRONG);
    }

    /**
     * Orders the tasks with the array given.
     */
    public function order(Request $request)
    {
        $order = 0;
        $data = $request->all();
        foreach ($data as $key => $value) {
            $item = Task::find($value[0]['id']);
            if (! $item) {
                return response()->json(false, 422);
            }
            $item->order = $order;
            $item->save();
            $order++;
        }

        return ResponseBuilder::success();
    }
}
