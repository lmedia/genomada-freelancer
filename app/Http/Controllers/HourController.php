<?php

namespace App\Http\Controllers;

use App\Hour;
use App\Income;
use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class HourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function show(Hour $hour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function edit(Hour $hour)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hour $hour)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hour $hour)
    {
        //
    }

    /**
     * Gets the hours list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetch()
    {
        $collection = [];
        foreach (Hour::where('start', '>=', Carbon::now()->subMonths(3))->orderBy('start', 'desc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'project_id' => $item->project_id,
                'client_id' => $item->project->client_id,
                'project' => $item->project->name,
                'hours' => $item->hours,
                'hours_show' => $item->hours.' h',
                'start' => $item->start,
                'start_show' => Carbon::parse($item->start)->format('d M Y - H:i').'h',
                'end' => $item->end,
                'end_show' => Carbon::parse($item->start)->format('d M Y - H:i').'h',
                'billable' => $item->billable,
                'pending' => $item->pending,
                'toggl_id' => $item->toggl_id,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Adds an array of hours.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $hours = $request->hours;
        foreach ($hours as $hour) {
            $process = true;
            if (isset($hour['selected']) and $hour['selected'] == 0) {
                $process = false;
            }
            if (! isset($hour['project_id'])) {
                $process = false;
            }

            // If we have to process this
            if ($process) {

                // Check if that hour ID was already added. If so, then update
                if ($hour['id'] > 0) {
                    $item = Hour::where('toggl_id', $hour['id'])->first();
                    if (! $item) {
                        $item = new Hour();
                    }
                } else {
                    $item = new Hour();
                }

                // Define hours
                $start = Carbon::parse($hour['start']);
                if (isset($hour['end'])) {
                    $end = Carbon::parse($hour['end']);
                } else {
                    $end = Carbon::parse($hour['start'])->addMinutes((int) $hour['minutes']);
                }
                $hours = round($end->diffInMinutes($start, true) / 60, 1);

                // Add the fields
                $item->name = (trim($hour['name']) == '') ? '-' : trim($hour['name']);
                $item->project_id = $hour['project_id'];
                $item->toggl_id = (isset($hour['id']) and $hour['id'] > 0) ? $hour['id'] : null;
                $item->start = $start->format('Y-m-d H:i:s');
                $item->end = $end->format('Y-m-d H:i:s');
                $item->hours = $hours;
                $item->billable = $hour['billable'];
                $item->save();

                // Store project toggl_id
                if (isset($hour['toggl_project_id']) and $hour['toggl_project_id'] > 0) {
                    $project = Project::find($item->project_id, ['id', 'toggl_id']);
                    $project->toggl_id = $hour['toggl_project_id'];
                    $project->save();
                }
            }
        }

        return ResponseBuilder::success();
    }

    /**
     * Fetches all the pending billing time entries.
     *
     * @return array[]
     */
    public function unbilledHours()
    {
        $entries = Hour::where('billable', '1')
            ->where('pending', '1')
            ->orderBy('project_id', 'asc')
            ->orderBy('name', 'asc')
            ->get();

        $items = new Collection();

        $lastId = 0;
        $lastProjectId = 0;
        $lastProject = '';
        $lastName = '';
        $lastHours = 0;
        $desired_price_hour = 0;

        foreach ($entries as $entry) {
            if ($lastProjectId != $entry->project_id || $lastName != trim($entry->name)) {
                if ($lastProjectId > 0 and $lastHours > 0) {
                    $desired_price_hour = DB::table('projects')->where('id', $lastProjectId)->value('desired_price_hour');
                    if ($desired_price_hour == 0) {
                        $desired_price_hour = DB::table('users')->where('id', auth()->user()->id)->value('desired_price_hour');
                    }
                    $data = [
                        'id' => $lastId,
                        'project_id' => $lastProjectId,
                        'store_project' => $lastProject,
                        'name' => $lastName,
                        'price_per_hour' => $desired_price_hour,
                        'hours' => round($lastHours, 2),
                        'total' => round($desired_price_hour * round($lastHours, 2)),
                    ];
                    $items->push($data);
                }

                $lastId = $entry->id;
                $lastProjectId = $entry->project_id;
                $lastProject = $entry->project->name;
                $lastName = trim($entry->name);
                $lastHours = 0;
                $desired_price_hour = DB::table('users')->where('id', auth()->user()->id)->value('desired_price_hour');
            }

            $lastHours += $entry->hours;
        }

        // Last iteration
        if ($lastId > 0) {
            $desired_price_hour = DB::table('projects')->where('id', $lastProjectId)->value('desired_price_hour');
            if ($desired_price_hour == 0) {
                $desired_price_hour = DB::table('users')->where('id', auth()->user()->id)->value('desired_price_hour');
            }

            $data = [
                'id' => $lastId,
                'project_id' => $lastProjectId,
                'store_project' => $lastProject,
                'name' => $lastName,
                'price_per_hour' => $desired_price_hour,
                'hours' => round($lastHours, 2),
                'total' => round($desired_price_hour * round($lastHours, 2)),
            ];
            $items->push($data);
        }

        return ResponseBuilder::success($items);
    }

    /**
     * Discards billing hours.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function discardBillingHours(Request $request)
    {
        foreach ($request->input('tasks') as $id) {
            $entry = Hour::findOrFail($id);
            if ($entry) {
                $entries = Hour::where('project_id', $entry->project_id)
                    ->where('name', trim($entry->name))
                    ->where('pending', '1')
                    ->get();
                foreach ($entries as $item) {
                    $item->pending = 0;
                    $item->save();
                }
            }
        }

        return ResponseBuilder::success();
    }

    /**
     * Updates the name of a task.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateNameBeforeBilling(Request $request)
    {
        $entry = Hour::findOrFail($request->input('id'));
        if ($entry) {
            $entries = Hour::where('project_id', $entry->project_id)
                ->where('name', trim($entry->name))
                ->where('pending', '1')
                ->get();
            foreach ($entries as $item) {
                $item->name = trim($request->input('name'));
                $item->save();
            }
        }

        return ResponseBuilder::success();
    }

    /**
     * Bill hours.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function billHours(Request $request)
    {

        // First, we mark them as billed
        foreach ($request->input('tasks') as $id) {
            $entry = Hour::findOrFail($id);
            if ($entry) {
                $entries = Hour::where('project_id', $entry->project_id)
                    ->where('name', trim($entry->name))
                    ->where('billable', '1')
                    ->where('pending', '1')
                    ->get();
                foreach ($entries as $item) {
                    $item->pending = 0;
                    $item->save();
                }
            }
        }

        // Then, we process the array of billed hours sent
        foreach ($request->input('toBill') as $slot) {
            foreach ($request->input('tasks') as $id) {

                // If it's selected...
                if ($slot['id'] == $id) {

                    // Store it in project incomes
                    $payment = new Income();
                    $payment->project_id = trim($slot['project_id']);
                    $payment->name = $slot['name'];
                    $payment->date = date('Y-m-d');
                    $payment->amount = $slot['total'];
                    $payment->billed_from_timer = 1;
                    $payment->save();
                }
            }
        }

        return ResponseBuilder::success();
    }
}
