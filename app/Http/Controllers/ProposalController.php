<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\Proposal;
use App\ProposalConcept;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class ProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'client_id' => 'required|exists:clients,id',
        ]);

        $item = new Proposal([
            'name' => trim($request->get('name')),
            'client_id' => $request->get('client_id'),
            'language_id' => $request->get('language_id'),
            'desc' => $request->get('desc'),
            'reference' => $request->get('reference'),
            'contact_person' => $request->get('contact_person'),
            'terms' => $request->get('terms'),
            'date' => Carbon::parse($request->get('date'))->format('Y-m-d'),

        ]);
        $item->save();

        // Store concepts
        $concepts = $request->get('concepts');
        foreach ($concepts as $elemento) {
            $concept = new ProposalConcept([
                'name' => $elemento['name'],
                'desc' => $elemento['desc'],
                'finish_date' => Carbon::parse($elemento['finish_date'])->format('Y-m-d'),
                'price' => $elemento['price'],
                'order' => 1,
            ]);
            $item->concepts()->save($concept);
        }

        return ResponseBuilder::success($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function show(Proposal $proposal)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function edit(Proposal $proposal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proposal $proposal)
    {
        $request->validate([
            'name' => 'required',
            'client_id' => 'required|exists:clients,id',
            'language_id' => 'required|exists:languages,id',
        ]);

        $item = Proposal::find($request->get('id'));
        if ($item) {
            $item->name = trim($request->get('name'));
            $item->client_id = $request->get('client_id');
            $item->language_id = $request->get('language_id');
            $item->desc = $request->get('desc');
            $item->date = Carbon::parse($request->get('date'))->format('Y-m-d');
            $item->terms = $request->get('terms');
            $item->reference = $request->get('reference');
            $item->contact_person = $request->get('contact_person');
            $item->save();

            // Remove old concepts to store new
            $item->concepts()->delete();
            $concepts = $request->get('concepts');
            foreach ($concepts as $elemento) {
                $concept = new ProposalConcept([
                    'name' => $elemento['name'],
                    'desc' => $elemento['desc'],
                    'finish_date' => Carbon::parse($elemento['finish_date'])->format('Y-m-d'),
                    'price' => $elemento['price'],
                    'order' => 1,
                ]);
                $item->concepts()->save($concept);
            }

            return ResponseBuilder::success($item);
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proposal $proposal)
    {
        //
    }

    /**
     * Gets the projects list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetch()
    {
        $collection = [];
        $user = Auth::user();
        foreach (Proposal::orderBy('date', 'desc')->orderBy('name', 'asc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'client' => $item->client->name,
                'total' => currency_format((string) $item->total, $user->currency->code),
                'reference' => $item->reference,
                'date' => Carbon::parse($item->date)->format('d/m/Y'),
                'url' => $item->url,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Returns a raw proposal or a blank one.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function fetchRaw(Request $request)
    {
        $item = Proposal::with(['concepts' => function ($query) {
            return $query->orderBy('order', 'asc');
        }])->find($request->id);
        if (! $item) {
            $item = new Proposal();
            $item->date = date('Y-m-d');
            $item->concepts = [];
        }

        return ResponseBuilder::success($item);
    }

    /**
     * Displays the proposal resource.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function viewProposal(Request $request)
    {
        $proposal = Proposal::where('id', $request->id)
                    ->where('slug', $request->slug)
                    ->with('client')
                    ->with('concepts')
                    //->with('taxes')
                    ->first();

        if (! $proposal) {
            return redirect()->to('https://genomada.com');
        }

        return view('proposals.view', compact('proposal'));
    }
}
