<?php

namespace App\Http\Controllers\Auth;

use App\Client;
use App\Http\Controllers\Controller;
use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get authenticated user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function current(Request $request)
    {
        return response()->json($request->user());
    }

    public function subscription(Request $request)
    {
        // Lifetime passes
        if ($request->user()->lifetime_pass) {
            $data = [
                'active' => true,
                'status' => null,
                'plan' => null,
                'updated_at' => null,
                'ends_at' => null,
                'trial_ends_at' => $request->user()->trial_ends_at,
                'projects' => Project::count(),
                'clients' => Client::count(),
            ];

            return response()->json($data);
        }

        // Regular cases
        $subscription = $request->user()->subscription('Genomada');

        if (! $subscription) {
            $data = [
                'active' => false,
                'status' => null,
                'plan' => null,
                'updated_at' => null,
                'ends_at' => null,
                'trial_ends_at' => $request->user()->trial_ends_at,
                'projects' => Project::count(),
                'clients' => Client::count(),
            ];

            return response()->json($data);
        }

        $data = [
            'active' => ($subscription->stripe_status == 'active') ? true : false,
            'active' => true,
            'status' => $subscription->stripe_status,
            'plan' => $subscription->stripe_plan,
            'updated_at' => Carbon::parse($subscription->updated_at)->format('Y-m-d H:i:s'),
            'ends_at' => $subscription->ends_at,
            'trial_ends_at' => $subscription->trial_ends_at,
            'projects' => Project::count(),
            'clients' => Client::count(),
        ];

        return response()->json($data);
    }
}
