<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $item = new Client([
            'name' => trim($request->get('name')),
            'contact' => trim($request->get('contact')),
            'email' => trim($request->get('email')),
            'phone' => trim($request->get('phone')),
            'vat_id' => trim($request->get('vat_id')),
            'address' => trim($request->get('address')),
            'city' => trim($request->get('city')),
            'zip' => trim($request->get('zip')),
            'country_id' => trim($request->get('country_id')),
            'notes' => trim($request->get('notes')),
        ]);
        $item->save();

        return ResponseBuilder::success($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        $item = Client::find($client->id);
        $user = Auth::user();
        $collection = [
            'id' => $item->id,
            'name' => $item->name,
            'total' => currency_format((string) $item->total, $user->currency->code),
            'hours' => $item->hours.'h',
            'price_hour' => $item->price_hour,
            'toggl_id' => $item->toggl_id,
        ];

        return ResponseBuilder::success($collection);
    }

    /**
     * Returns a raw client or a blank one.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function fetchRaw(Request $request)
    {
        $item = Client::find($request->id);
        if (! $item) {
            $item = new Client();
        }

        return ResponseBuilder::success($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $item = Client::find($request->get('id'));
        if ($item) {
            $item->name = trim($request->get('name'));
            $item->contact = trim($request->get('contact'));
            $item->email = trim($request->get('email'));
            $item->phone = trim($request->get('phone'));
            $item->vat_id = trim($request->get('vat_id'));
            $item->address = trim($request->get('address'));
            $item->city = trim($request->get('city'));
            $item->zip = trim($request->get('zip'));
            $item->country_id = trim($request->get('country_id'));
            $item->notes = trim($request->get('notes'));
            $item->save();

            return ResponseBuilder::success($item);
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }

    /**
     * Gets the projects list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchForSelect()
    {
        $collection = [];
        foreach (Client::orderBy('name', 'asc')->get() as $item) {
            array_push($collection, [
                'value' => $item->id,
                'text' => $item->name,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Gets the projects list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetch()
    {
        $collection = [];
        $user = Auth::user();
        foreach (Client::orderBy('name', 'asc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'total' => currency_format((string) $item->total, $user->currency->code),
                'hours' => $item->hours.'h',
                'price_hour' => $item->price_hour,
                'toggl_id' => $item->toggl_id,
            ]);
        }

        return ResponseBuilder::success($collection);
    }
}
