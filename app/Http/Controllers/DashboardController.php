<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\Holiday;
use App\Mit;
use App\Project;
use App\Task;
use App\Workload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Gets the MIT's for the current week.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchWeek(Request $request)
    {
        $collection = [];

        $which = $request->get('which');
        if ($which == 'previous') {
            $monday = Carbon::now()->subWeek()->startOfWeek();
            $week_title = __('genomada.last_week');
        } elseif ($which == 'next') {
            $monday = Carbon::now()->addWeek()->startOfWeek();
            $week_title = __('genomada.next_week');
        } else {
            $monday = Carbon::now()->startOfWeek();
            $week_title = __('genomada.this_week');
        }

        for ($columna = 0; $columna < 6; $columna++) {
            if ($columna == 0) {
                $name = __('genomada.monday');
            } elseif ($columna == 1) {
                $name = __('genomada.tuesday');
            } elseif ($columna == 2) {
                $name = __('genomada.wednesday');
            } elseif ($columna == 3) {
                $name = __('genomada.thursday');
            } elseif ($columna == 4) {
                $name = __('genomada.friday');
            } else {
                $name = __('genomada.weekend');
            }

            if ($columna > 0) {
                $day = $monday->add(1, 'days')->format('Y-m-d');
            } else {
                $day = $monday->format('Y-m-d');
            }

            $thisColumn = [];
            foreach (Mit::where('day', $day)->orderBy('order', 'asc')->get() as $item) {
                array_push($thisColumn, [
                    'id' => $item->id,
                    'name' => $item->name,
                    'completed' => $item->completed,
                ]);
            }

            $name = $name.' '.date('d', strtotime($day));
            if ($columna == 5) {
                $name = __('genomada.weekend');
            }

            $newTask = [
                'name' => '',
                'day' => $day,
                'completed' => 0,
                'order' => 0,
            ];

            // Workloads
            $works = [];
            foreach (Workload::where('day', $day)->get() as $item) {
                array_push($works, [
                    'project' => $item->project->name,
                    'hours' => $item->hours,
                ]);
            }

            // Holiday
            $holiday = false;
            if (Holiday::where('day', $day)->count() > 0) {
                $holiday = true;
            }
            if ($columna == 5) {
                $holiday = true;
            }

            array_push($collection, [
                'week_title' => $week_title,
                'column' => $columna,
                'name' => $name,
                'day' => $day,
                'mits' => $thisColumn,
                'adding' => false,
                'workloads' => $works,
                'holiday' => $holiday,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Gets the overview of all.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchDashboardOverview(Request $request)
    {
        $user = Auth::user();

        // Average price hour and others
        $total_projects = 0;
        $average_price_hour = 0;
        $estimated_earnings_pending = 0;
        $total_benefit = 0;

        $focused = 0;
        $unfocused = 0;
        $archived = 0;
        $active = 0;
        $finished = 0;

        $projects = Project::get();
        if ($projects->count() == 0) {
            return ResponseBuilder::success(null);
        }

        foreach ($projects as $project) {
            if ($project->type != 'free') {
                $average_price_hour = $average_price_hour + $project->price_hour;
                $total_projects++;
            }
            $estimated_earnings_pending += $project->estimated_earnings_pending;
            $total_benefit += $project->benefit;

            if ($project->finished == 1) {
                $finished++;
            } else {
                if ($project->archived == 0 and $project->focus == 1) {
                    $focused++;
                }
                if ($project->archived == 0 and $project->focus == 0) {
                    $unfocused++;
                }
                if ($project->archived == 1) {
                    $archived++;
                }
                if ($project->archived == 0) {
                    $active++;
                }
            }
        }

        // ****************************************************************************
        // Best projects ever
        // ****************************************************************************
        $best_projects = Project::where('type', '!=', 'free')->get();
        if ($best_projects->count() > 0) {
            $array = [];
            foreach ($best_projects as $item) {
                array_push($array, [
                    'name' => $item->name,
                    'client' => $item->client->name,
                    'price_hour' => $item->price_hour,
                    'benefit' => $item->benefit,
                ]);
            }

            $sortArray = [];

            foreach ($array as $item) {
                foreach ($item as $key => $value) {
                    if (! isset($sortArray[$key])) {
                        $sortArray[$key] = [];
                    }
                    $sortArray[$key][] = $value;
                }
            }
            $orderby = 'price_hour';
            array_multisort($sortArray[$orderby], SORT_DESC, $array);

            $limit = 50;
            $user = Auth::user();

            // Prepare array of data
            $counter = 0;
            $collection_best_projects = [];
            foreach ($array as $item) {
                if ($counter < $limit && $item['price_hour'] > 0) {
                    array_push($collection_best_projects, [
                        'client' => $item['name'],
                        'benefit' => currency_format($item['benefit'], $user->currency->code),
                        'price_hour' => currency_format($item['price_hour'], $user->currency->code),
                    ]);
                    $counter++;
                }
            }
        } else {
            $collection_best_projects = [];
        }

        // ****************************************************************************
        // ****************************************************************************

        $collection = [
            'projects_focused' => $focused,
            'projects_unfocused' => $unfocused,
            'projects_archived' => $archived,
            'projects_active' => $active,
            'projects_finished' => $finished,
            'price_hour' => currency_format((string) round($average_price_hour / $total_projects, 2), $user->currency->code),
            'desired_price_hour' =>currency_format((string) config('genomada.desired_price_hour'), $user->currency->code),
            'estimated_earnings_pending' => currency_format((string) $estimated_earnings_pending, $user->currency->code),
            'total_benefit' => currency_format((string) $total_benefit, $user->currency->code),
            'best_projects_ever' => $collection_best_projects,
        ];

        return ResponseBuilder::success($collection);
    }

    /**
     * Gets the tasks.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchTasks(Request $request)
    {
        $collection = [];

        $projects = Project::where('focus', '1')->where('archived', '0')->orderBy('order', 'asc')->orderBy('name', 'asc')->get(['id', 'name']);
        if ($projects->count() == 0) {
            return ResponseBuilder::success(null);
        }

        foreach ($projects as $project) {
            $tasks = Task::where('project_id', $project->id)
                        ->where('focus', '1')
                        ->where('completed', '0')
                        ->orderBy('order', 'asc')
                        ->orderBy('name', 'asc')
                        ->get(['id', 'name', 'due_date']);
            foreach ($tasks as $task) {
                array_push($collection, [
                    'id' => $task->id,
                    'name' => $task->name,
                    'due_date' => $task->due_date,
                    'show_formatted_due_date' => $task->show_formatted_due_date,
                    'project' => $project->name,
                    'project_id' => $project->id,
                ]);
            }
        }

        return ResponseBuilder::success($collection);
    }
}
