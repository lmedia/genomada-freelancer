<?php

namespace App\Http\Controllers\Integrations\Toggl;

use App\Hour;
use App\Http\Controllers\Controller;
use App\IntegrationToggl;
use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use App\Integrations\Toggl\TogglApi;
use App\Integrations\Toggl\TogglReportsApi;

class TogglController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Get the projects.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProjects(Request $request)
    {
        $force_reload = false;
        if ($request->force == 1) {
            $force_reload = true;
        }

        $user = auth()->user();
        if ($user->token_toggl) {
            if ($force_reload) {
                IntegrationToggl::where('user_id', $user->id)->delete();
            }

            // Load from cache
            $cached = IntegrationToggl::first();
            if ($cached and ! $force_reload) {
                return ResponseBuilder::success(json_decode($cached->projects, true));
            }

            $collection = [];
            $toggl = new TogglApi($user->token_toggl);
            $clients = $toggl->getClients();
            $clients = json_decode(json_encode($clients), true);
            if (is_array($clients)) {
                foreach ($clients as $client) {
                    if (is_array($client) and $client['id'] > 0) {
                        $projects = $toggl->getClientProjects($client['id']);
                        $projects = json_decode(json_encode($projects), true);
                        if (is_array($projects)) {
                            foreach ($projects as $project) {
                                if (is_array($project) and $project['id'] > 0) {
                                    array_push($collection, [
                                        'id' => $project['id'],
                                        'name' => $project['name'],
                                        'client_id' => $client['id'],
                                        'client_name' => $client['name'],
                                        'value' => $project['id'],
                                        'text' => $client['name'].' / '.$project['name'],
                                    ]);
                                }
                            }
                        }
                    }
                }
            }

            $cache = new IntegrationToggl();
            $cache->projects = json_encode($collection);
            $cache->save();

            return ResponseBuilder::success($collection);
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Get the project hours.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProjectHours(Request $request)
    {
        $form = $request->form;
        $user = auth()->user();
        if ($user->token_toggl) {
            $project = Project::find($form['projectId']);
            if ($project) {
                $collection = [];

                $toggl = new TogglApi($user->token_toggl);
                $workspaces = $toggl->getWorkspaces();
                $workspaces = json_decode(json_encode($workspaces), true);
                if ($workspaces[0]) {
                    $toggl = new TogglReportsApi($user->token_toggl);
                    $data = [
                        'user_agent' => 'Genomada',
                        'workspace_id' => $workspaces[0]['id'],
                        'since' => $project->created_at->format('Y-m-d'),
                        'until' => date('Y-m-d'),
                    ];
                    $items = $toggl->getDetailsReport($data);
                    $items = json_decode(json_encode($items), true);
                    if (is_array($items)) {
                        foreach ($items as $item) {
                            if ($item['pid'] == $form['toggl_project_id']) {

                                // Check if already exists
                                $exists = Hour::where('toggl_id', trim($item['id']))->first();
                                if (! $exists) {
                                    $start = Carbon::parse($item['start']);
                                    $end = Carbon::parse($item['end']);
                                    $hours = round($end->diffInMinutes($start, true) / 60, 1);

                                    array_push($collection, [
                                        'id' => $item['id'],
                                        'project_id' => $project->id,
                                        'toggl_project_id' => $item['pid'],
                                        'name' => $item['description'],
                                        'start' => $start->format('Y-m-d H:i:s'),
                                        'start_show' => $start->format('d M H:i:s'),
                                        'end' => $end->format('Y-m-d H:i:s'),
                                        'end_show' => $end->format('d M H:i:s'),
                                        'hours' => $hours,
                                        'billable' => ($project->type == 'hours') ? 1 : 0,
                                        'value' => $item['id'],
                                    ]);
                                }
                            }
                        }

                        return ResponseBuilder::success($collection);
                    }
                }
            }
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Get the project hours.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHours(Request $request)
    {
        $form = $request->form;
        $user = auth()->user();
        if ($user->token_toggl) {
            $collection = [];

            $toggl = new TogglApi($user->token_toggl);
            $time_entries = $toggl->getTimeEntries();
            $time_entries = json_decode(json_encode($time_entries), true);
            if (count($time_entries) > 0) {
                foreach ($time_entries as $item) {

                    // Check if already exists
                    $exists = Hour::where('toggl_id', trim($item['id']))->first();
                    if (! $exists) {
                        $start = Carbon::parse($item['start']);
                        $end = Carbon::parse($item['stop']);

                        $project_id = null;
                        $billable = 0;
                        $checked = 0;
                        if (isset($item['pid'])) {
                            $proj = Project::where('toggl_id', $item['pid'])->first(['id', 'type']);
                            if ($proj) {
                                $project_id = $proj->id;
                                $checked = 1;
                                if ($proj->type == 'hours') {
                                    $billable = 1;
                                }
                            }
                        }

                        array_push($collection, [
                            'id' => $item['id'],
                            'selected' => $checked,
                            'project_id' => $project_id,
                            'toggl_project_id' => $item['pid'],
                            'name' => (isset($item['description'])) ? $item['description'] : '',
                            'start' => $start->format('Y-m-d H:i:s'),
                            'start_show' => $start->format('d M H:i:s'),
                            'end' => $end->format('Y-m-d H:i:s'),
                            'end_show' => $end->format('d M H:i:s'),
                            'hours' => round($item['duration'] / 60 / 60, 1),
                            'hours_show' => round($item['duration'] / 60 / 60, 1).' h',
                            'value' => $item['id'],
                            'billable' => $billable,
                        ]);
                    }
                }

                return ResponseBuilder::success($collection);
            }
        }

        return ResponseBuilder::error(250);
    }
}
