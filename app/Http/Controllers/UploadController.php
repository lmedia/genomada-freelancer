<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;
use MediaUploader;
use Plank\Mediable\Media;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Uploads some files.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uploadFiles(Request $request, $taskId)
    {
        $item = Task::find($taskId);
        if ($item) {
            foreach ($request->files as $file) {
                $fName = $file->getClientOriginalName();
                $nameFinal = Str::slug(pathinfo($fName, PATHINFO_FILENAME), '-');
                $media = MediaUploader::fromSource($file)
                    ->useFilename($nameFinal)
                    ->toDestination('local', 'genomada-taskfiles/'.Auth::id().'/'.$item->id)
                    ->upload();
                $item->attachMedia($media, ['taskfile']);
            }

            return ResponseBuilder::success();
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Fetches a file.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetchFile(Request $request, $mediableId, $mediaId)
    {
        $task = Task::find($mediableId);
        if (! $task) {
            return response()->json(null, 422);
        }
        $media = Media::find($mediaId);
        $filename = $media->directory.'/'.$media->filename.'.'.$media->extension;
        if (Storage::disk('local')->exists($filename)) {
            return Storage::disk('local')->download($filename);
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Shows a file.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetchFileShowImage(Request $request, $mediableId, $mediaId)
    {
        $task = Task::find($mediableId);
        if (! $task) {
            return response()->json(null, 422);
        }
        $media = Media::find($mediaId);
        $filename = $media->directory.'/'.$media->filename.'.'.$media->extension;
        if (Storage::disk('local')->exists($filename)) {
            return Storage::disk('local')->get($filename);
        }

        return ResponseBuilder::error(250);
    }

    /**
     * Deletes a file.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFile(Request $request)
    {
        $media = Media::find($request->id);
        $media->forceDelete();

        return ResponseBuilder::success();
    }
}
