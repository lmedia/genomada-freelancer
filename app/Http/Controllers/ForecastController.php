<?php

namespace App\Http\Controllers;

use App\Holiday;
use App\Income;
use App\Project;
use App\Proposal;
use App\Workload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class ForecastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function show(Proposal $proposal)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function edit(Proposal $proposal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proposal $proposal)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proposal $proposal)
    {
        //
    }

    /**
     * Gets the projects list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetch()
    {
        $forecast_months = 6;
        $max_hours_day = 6;

        $projects = [];
        foreach (Project::where('finished', 0)->orderBy('name', 'asc')->get(['id', 'name']) as $item) {
            array_push($projects, [
                'value' => $item->id,
                'text' => $item->name,
            ]);
        }

        $calendar = [];
        $date = Carbon::now()->startOfWeek()->subDay(1);
        $dia = 0;
        $month = [];
        for ($days = 0; $days < ($forecast_months * 4 * 7); $days++) {
            if ($dia == 28) {
                array_push($calendar, $month);
                $dia = 0;
                $month = [];
            }
            $date->addDay(1);
            $holiday = false;
            if ($date->isWeekend()) {
                $holiday = true;
            }
            if (Holiday::where('day', $date->format('Y-m-d'))->count() > 0) {
                $holiday = true;
            }

            $wkload = [];
            if (! $holiday) {
                $workloads = Workload::where('day', $date->format('Y-m-d'))->get();
                if ($workloads) {
                    $the_hours_yet = 0;
                    foreach ($workloads as $workload) {
                        $project_name = $workload->project->name;
                        $the_hours_yet += $workload->hours;
                        $overloaded = false;
                        if ($the_hours_yet > $max_hours_day) {
                            $overloaded = true;
                        }
                        $data = [
                            'id' => $workload->id,
                            'project' => $project_name,
                            'project_id' => $workload->project_id,
                            'label' => $project_name.' ('.$workload->hours.'h)',
                            'hours' => $workload->hours,
                            'overloaded' => $overloaded,
                        ];
                        array_push($wkload, $data);
                    }
                }
            }
            $day = $date->format('d');
            $day++;
            $day--;
            $mnth = $date->format('m');
            $mnth++;
            $mnth--;
            array_push($month, [
                'date' => $date->format('Y-m-d'),
                'show_date' => $date->format('d/m/Y'),
                'day' => $day,
                'month' => $mnth,
                'year' => $date->format('Y'),
                'name' => $date->format('d').' '.$date->format('F').' '.$date->format('Y'),
                'holiday' => $holiday,
                'workloads' => $wkload,
                'today' => ($date->format('Y-m-d') == date('Y-m-d') ? true : false),
            ]);
            $dia++;
        }
        array_push($calendar, $month);

        $toReturn = [
            'projects' => $projects,
            'calendar' => $calendar,
        ];

        return ResponseBuilder::success($toReturn);
    }

    /**
     * Adds workload of holiday.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if ($request->type == 'workload') {
            $item = new Workload();
            $item->project_id = $request->projId;
            $item->day = Carbon::parse($request->date);
            $item->hours = $request->hours;
            $item->save();
        }

        if ($request->type == 'holiday') {
            $item = new Holiday();
            $item->day = Carbon::parse($request->date);
            $item->save();
        }

        return ResponseBuilder::success();
    }

    /**
     * Removes a workload.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hour  $hour
     * @return \Illuminate\Http\Response
     */
    public function removeWorkload(Request $request)
    {
        if ($request->id > 0) {
            $id = $request->id;
            $id++;
            $id--;
            $item = Workload::find($id);
            $item->delete();
        }

        return ResponseBuilder::success();
    }




    /**
     * Gets the data for schedule
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function schedule()
    {
        $projects = [];
        $total_incomes = 0;
        $total_outcomes = 0;
        $estimated_earnings_pending = 0;
        foreach (Project::orderBy('name', 'asc')->get() as $item) {

            $incomes = $item->incomes()->whereBetween('date', [Carbon::now()->startOfYear()->format('Y-m-d'), Carbon::now()->endOfYear()->format('Y-m-d')])->orderBy('date', 'asc')->sum('amount');
            $outcomes = $item->outcomes()->whereBetween('date', [Carbon::now()->startOfYear()->format('Y-m-d'), Carbon::now()->endOfYear()->format('Y-m-d')])->orderBy('date', 'asc')->sum('amount');

            if ($incomes > 0 or $outcomes > 0 or $item->estimated_earnings_pending > 0) {

                array_push($projects, [
                    'name' => $item->name,
                    'total' => $item->total,
                    'incomes' => $incomes,
                    'outcomes' => $outcomes,
                    'pending' => $item->estimated_earnings_pending,
                    'total_show' => currency($item->total),
                    'incomes_show' => currency($incomes),
                    'outcomes_show' => currency($outcomes),
                    'pending_show' => currency($item->estimated_earnings_pending),
                ]);

                $total_incomes+= $incomes;
                $total_outcomes+= $outcomes;
                if ($item->finished == 0) $estimated_earnings_pending+= $item->estimated_earnings_pending;

            }

        }

        // Calculations
        $yearly_goal = config('genomada.yearly_revenue_goal');
        $balance_so_far = $total_incomes - $total_outcomes;
        $gap_real = $yearly_goal - $balance_so_far;
        $gap_without_estimated = $yearly_goal - $balance_so_far - $estimated_earnings_pending;

        $toReturn = [
            'projects' => $projects,
            'total_incomes' => $total_incomes,
            'total_outcomes' => $total_outcomes,
            'balance_so_far' => $balance_so_far,
            'pending' => $estimated_earnings_pending,
            'yearly_goal' => $yearly_goal,
            'gap_real' => $gap_real,
            'gap_without_estimated' => $gap_without_estimated,
            'total_incomes_show' => currency($total_incomes),
            'total_outcomes_show' => currency($total_outcomes),
            'balance_so_far_show' => currency($balance_so_far),
            'pending_show' => currency($estimated_earnings_pending),
            'yearly_goal_show' => currency($yearly_goal),
            'gap_real_show' => currency($gap_real),
            'gap_without_estimated_show' => currency($gap_without_estimated),
        ];

        return ResponseBuilder::success($toReturn);
    }
}
