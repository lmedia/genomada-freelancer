<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\Tax;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Gets the taxes list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetch()
    {
        $collection = [];
        foreach (Tax::orderBy('order', 'asc')->orderBy('name', 'asc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'percent' => $item->percent,
                'order' => $item->order,
            ]);
        }

        return ResponseBuilder::success($collection);
    }

    /**
     * Updates the taxes.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function updateTaxes(Request $request)
    {
        $user = User::find(Auth::id());
        if (! $user) {
            return ResponseBuilder::error(250);
        }

        $user->taxes()->delete();
        foreach ($request->taxes as $item) {
            $percent = $item['percent'];
            $percent++;
            $percent--;
            $order = $item['order'];
            $order++;
            $order--;

            $tax = new Tax();
            $tax->name = $item['name'];
            $tax->percent = $percent;
            $tax->order = $order;
            $tax->user_id = $user->id;
            $tax->save();
        }

        $collection = [];
        foreach (Tax::orderBy('order', 'asc')->orderBy('name', 'asc')->get() as $item) {
            array_push($collection, [
                'id' => $item->id,
                'name' => $item->name,
                'percent' => $item->percent,
                'order' => $item->order,
            ]);
        }

        return ResponseBuilder::success($collection);
    }
}
