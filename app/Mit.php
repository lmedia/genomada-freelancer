<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mit extends Model
{
    use HasFactory;
    use Multitenantable;

    protected $fillable = [
        'name', 'day', 'order', 'completed',
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }
}
