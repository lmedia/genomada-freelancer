<?php

namespace App;

use App\Traits\Multitenantable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Workload extends Model
{
    use Multitenantable;

    protected $fillable = [
        'project_id', 'day', 'hours',
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }

    /**
     * Get the project.
     */
    public function project()
    {
        return $this->belongsTo(\App\Project::class)->withDefault();
    }
}
