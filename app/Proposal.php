<?php

namespace App;

use App\Traits\Multitenantable;
use App\Traits\Sluggable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    use HasFactory;
    use Multitenantable;
    use Sluggable;

    protected $fillable = [
        'name', 'client_id', 'language_id', 'date', 'reference', 'contact_person', 'desc', 'terms',
    ];

    public function sluggable()
    {
        return [
            'source' => 'name',
        ];
    }

    /**
     * The concepts.
     */
    public function concepts()
    {
        return $this->hasMany(\App\ProposalConcept::class);
    }

    /**
     * Get the project.
     */
    public function language()
    {
        return $this->belongsTo(\App\Language::class)->withDefault();
    }

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }

    /**
     * Get the client.
     */
    public function client()
    {
        return $this->belongsTo(\App\Client::class)->withDefault();
    }

    /*
    Attributes
    */
    public function getTotalAttribute()
    {
        $total = 0;
        foreach ($this->concepts()->get() as $item) {
            $total += $item->price;
        }

        return $total;
    }

    public function getUrlAttribute()
    {
        return '/proposals/'.$this->id.'/'.$this->slug;
    }

    public function getShowDateAttribute()
    {
        return Carbon::parse($this->date)->format('d/m/Y');
    }
}
