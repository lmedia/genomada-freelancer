<?php

namespace App;

use App\Traits\Multitenantable;
use App\Traits\EncryptableDbAttribute;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Task extends Model
{
    use HasFactory;
    use Multitenantable;
    use Mediable;
    use EncryptableDbAttribute;

    /** @var array The attributes that should be encrypted/decrypted */
    protected $encryptable = [
        'name',
        'desc',
    ];

    protected $fillable = [
        'name', 'project_id', 'desc', 'focus', 'completed', 'due_date', 'inbox', 'order',
        'toggl_id', 'board_id',
    ];

    protected $appends = [
        'excerpt',
        'media_count',
        'show_due_date',
        'show_formatted_due_date',
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class)->withDefault();
    }

    /**
     * Get the project.
     */
    public function project()
    {
        return $this->belongsTo(\App\Project::class)->withDefault();
    }

    /**
     * Get the board.
     */
    public function board()
    {
        return $this->belongsTo(\App\Board::class)->withDefault();
    }

    /**
     * Accessors.
     */
    public function getExcerptAttribute()
    {
        $chars = 100;
        $str = trim(strip_tags($this->desc));
        if (strlen($str) == 0) {
            return ' ';
        }
        if (strlen($str) > $chars) {
            return strip_tags(trim(substr($str, 0, $chars))).' [...]';
        } else {
            return strip_tags($str);
        }
    }

    public function getMediaCountAttribute()
    {
        return $this->media()->count();
    }

    public function getShowDueDateAttribute()
    {
        if ($this->due_date) {
            return Carbon::parse($this->due_date)->format('d/m/Y');
        } else {
            return '-';
        }
    }

    public function getShowFormattedDueDateAttribute()
    {
        if ($this->due_date == null) {
            return '';
        }

        if ($this->due_date <= date('Y-m-d')) {
            return '
                <span class="badge-due-date text-danger"> 
                    <img src="/img/dot-red.svg"/>
                    '.$this->show_due_date.' 
                </span>
            ';
        } else {
            return '
                <span class="badge-due-date"> 
                    <img src="/img/dot-blue.svg"/>
                    '.$this->show_due_date.' 
                </span>
            ';
        }
    }
}
