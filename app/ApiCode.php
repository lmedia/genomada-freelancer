<?php

namespace App;

class ApiCode
{
    public const SOMETHING_WENT_WRONG = 250;
    public const CLIENT_ALREADY_EXISTS = 251;
}
