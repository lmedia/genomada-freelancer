<?php

namespace App\Console\Commands;

use App\Jobs\SyncToggl;
use App\User;
use Illuminate\Console\Command;

class TriggerSyncToggl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:toggl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Toggl users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::where('token_toggl', '!=', '')->get();
        foreach ($users as $user) {
            dispatch(new SyncToggl($user));
        }
    }
}
