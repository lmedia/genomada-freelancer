<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| SPA Routes
|--------------------------------------------------------------------------
|
| Here is where you can register SPA routes for your frontend. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "spa" middleware group.
|
*/

Route::get('/proposals/{id}/{slug}', '\App\Http\Controllers\ProposalController@viewProposal')->name('proposal.view');

Route::get('/project/thumbnail/{id}', '\App\Http\Controllers\ProjectController@getThumbnail')->name('projects.thumbnail');

Route::get('{path}', '\App\Http\Controllers\SpaController')->where('path', '(.*)');
