<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::get('/user', '\App\Http\Controllers\Auth\UserController@current');
    Route::get('/subscription', '\App\Http\Controllers\Auth\UserController@subscription');

    Route::patch('settings/profile', '\App\Http\Controllers\Settings\ProfileController@update');
    Route::patch('settings/password', '\App\Http\Controllers\Settings\PasswordController@update');
    Route::patch('settings/integrations', '\App\Http\Controllers\Settings\ProfileController@updateIntegrations');
    Route::patch('settings/account', '\App\Http\Controllers\Settings\ProfileController@updateAccount');

    Route::post('/dashboard/week', '\App\Http\Controllers\DashboardController@fetchWeek')->name('dashboard.week');
    Route::get('/dashboard/overview', '\App\Http\Controllers\DashboardController@fetchDashboardOverview')->name('dashboard.overview');
    Route::get('/dashboard/tasks', '\App\Http\Controllers\DashboardController@fetchTasks')->name('dashboard.tasks');

    Route::resource('projects', '\App\Http\Controllers\ProjectController');
    Route::get('/project/counters', '\App\Http\Controllers\ProjectController@projectsCounters')->name('projects.counters');
    Route::get('/fetch/projects', '\App\Http\Controllers\ProjectController@fetch')->name('projects.fetch');
    Route::get('/fetch/projects-active', '\App\Http\Controllers\ProjectController@fetchActive')->name('projects.fetchActive');
    Route::post('/update/project/focus', '\App\Http\Controllers\ProjectController@toggleFocus')->name('projects.update.focus');
    Route::post('/update/project/archive', '\App\Http\Controllers\ProjectController@toggleArchive')->name('projects.update.archive');
    Route::post('/update/project/finish', '\App\Http\Controllers\ProjectController@toggleFinish')->name('projects.update.finishs');
    Route::post('/order/projects', '\App\Http\Controllers\ProjectController@order')->name('projects.order');
    Route::post('/project/raw', '\App\Http\Controllers\ProjectController@fetchRaw')->name('projects.raw');
    Route::post('/project/hours', '\App\Http\Controllers\ProjectController@fetchHours')->name('projects.hours');
    Route::post('/project/incomes', '\App\Http\Controllers\ProjectController@fetchIncomes')->name('projects.incomes');
    Route::post('/project/outcomes', '\App\Http\Controllers\ProjectController@fetchOutcomes')->name('projects.outcomes');
    Route::post('/project/image', '\App\Http\Controllers\ProjectController@setImage')->name('project.image');

    Route::resource('proposals', '\App\Http\Controllers\ProposalController');
    Route::resource('proposal-concepts', '\App\Http\Controllers\ProposalConceptController');
    Route::get('/fetch/proposals', '\App\Http\Controllers\ProposalController@fetch')->name('proposals.fetch');
    Route::post('/proposal/raw', '\App\Http\Controllers\ProposalController@fetchRaw')->name('proposal.raw');

    Route::resource('clients', '\App\Http\Controllers\ClientController');
    Route::get('/fetch/clients', '\App\Http\Controllers\ClientController@fetch')->name('clients.fetch');
    Route::get('/fetch/select/clients', '\App\Http\Controllers\ClientController@fetchForSelect')->name('clients.fetch.select');
    Route::post('/client/raw', '\App\Http\Controllers\ClientController@fetchRaw')->name('client.raw');

    Route::resource('tasks', '\App\Http\Controllers\TaskController');
    Route::post('/fetch/projects/tasks', '\App\Http\Controllers\TaskController@fetch')->name('project.tasks.fetch');
    Route::post('/order/tasks', '\App\Http\Controllers\TaskController@order')->name('project.tasks.order');
    Route::post('/update/task/focus', '\App\Http\Controllers\TaskController@toggleFocus')->name('tasks.update.focus');
    Route::post('/update/task/completed', '\App\Http\Controllers\TaskController@toggleCompleted')->name('tasks.update.completed');
    Route::post('/update/task/board', '\App\Http\Controllers\TaskController@storeBoard')->name('tasks.update.board');

    Route::resource('boards', '\App\Http\Controllers\BoardController');
    Route::post('/fetch/boards', '\App\Http\Controllers\BoardController@fetchBoards')->name('boards.get');
    Route::post('/task/kanban', '\App\Http\Controllers\BoardController@loadKanban')->name('kanban');
    Route::post('/task/kanban/save', '\App\Http\Controllers\BoardController@saveKanBan')->name('kanban.save');
    Route::post('/task/kanban/delete/panel', '\App\Http\Controllers\BoardController@destroy')->name('kanban.panel.delete');
    Route::post('/task/kanban/rename/panel', '\App\Http\Controllers\BoardController@renamePanel')->name('kanban.panel.rename');
    Route::post('/task/kanban/fetch', '\App\Http\Controllers\BoardController@loadTaskForKanBan')->name('kanban.task.load');
    Route::post('/task/kanban/create', '\App\Http\Controllers\BoardController@createTaskFromKanBan')->name('kanban.task.create');

    Route::resource('mits', '\App\Http\Controllers\MitController');
    Route::post('/mits/order', '\App\Http\Controllers\MitController@order')->name('mits.order');
    Route::post('/update/mit/completed', '\App\Http\Controllers\MitController@toggleCompleted')->name('mit.update.completed');

    Route::resource('uploads', '\App\Http\Controllers\UploadController');
    Route::post('/upload/files/{taskId}', '\App\Http\Controllers\UploadController@uploadFiles')->name('upload.files');
    Route::post('/upload/file/delete', '\App\Http\Controllers\UploadController@deleteFile')->name('upload.file.delete');
    Route::get('/upload/file/get/{mediableId}/{mediaId}', '\App\Http\Controllers\UploadController@fetchFile')->name('upload.file.fetch');
    Route::get('/upload/file/show/{mediableId}/{mediaId}', '\App\Http\Controllers\UploadController@fetchFileShowImage')->name('upload.file.show');

    Route::resource('hours', '\App\Http\Controllers\HourController');
    Route::post('/hours/add', '\App\Http\Controllers\HourController@add')->name('hours.add');
    Route::get('/fetch/hours', '\App\Http\Controllers\HourController@fetch')->name('hours.fetch');

    Route::resource('incomes', '\App\Http\Controllers\IncomeController');

    Route::resource('langauges', '\App\Http\Controllers\LanguageController');
    Route::get('/fetch/languages', '\App\Http\Controllers\LanguageController@fetch')->name('languages.fetch');

    // Forecast & Schedule
    Route::get('/forecast', '\App\Http\Controllers\ForecastController@fetch')->name('forecast.fetch');
    Route::post('/forecast/add', '\App\Http\Controllers\ForecastController@add')->name('forecast.add');
    Route::post('/forecast/delete-workload', '\App\Http\Controllers\ForecastController@removeWorkload')->name('forecast.delete-workload');
    Route::get('/schedule', '\App\Http\Controllers\ForecastController@schedule')->name('schedule');

    // Billing hours
    Route::get('/billing/hours/unbilled', '\App\Http\Controllers\HourController@unbilledHours')->name('hours.unbilled');
    Route::post('/billing/hours/updatename', '\App\Http\Controllers\HourController@updateNameBeforeBilling')->name('hours.updatename');
    Route::post('/billing/hours/discard', '\App\Http\Controllers\HourController@discardBillingHours')->name('hours.discard');
    Route::post('/billing/hours/bill', '\App\Http\Controllers\HourController@billHours')->name('hours.bill');

    Route::resource('outcomes', '\App\Http\Controllers\OutcomeController');

    // Countries
    Route::resource('countries', '\App\Http\Controllers\CountryController');
    Route::get('/fetch/countries', '\App\Http\Controllers\CountryController@fetch')->name('countries.fetch');

    // Timezones
    Route::get('/fetch/timezones', '\App\Http\Controllers\TimezoneController@fetch')->name('timezones.fetch');

    // Currencies
    Route::get('/fetch/currencies', '\App\Http\Controllers\CurrencyController@fetch')->name('currencies.fetch');

    // Taxes
    Route::get('/fetch/taxes', '\App\Http\Controllers\TaxController@fetch')->name('taxes.fetch');
    Route::post('/tax/update-taxes', '\App\Http\Controllers\TaxController@updateTaxes')->name('tax.update');

    // Analytics
    Route::get('/analytics/best-projects', '\App\Http\Controllers\BestProjectsController@index');
    Route::get('/analytics/monthly-earnings', '\App\Http\Controllers\MonthlyEarningsController@index');

    // Stripe
    Route::get('/user/setup-intent', '\App\Http\Controllers\API\UserController@getSetupIntent');
    Route::put('/user/subscription', '\App\Http\Controllers\API\UserController@updateSubscription');
    Route::post('/user/payments', '\App\Http\Controllers\API\UserController@postPaymentMethods');
    Route::get('/user/payment-methods', '\App\Http\Controllers\API\UserController@getPaymentMethods');
    Route::post('/user/remove-payment', '\App\Http\Controllers\API\UserController@removePaymentMethod');
    Route::post('/user/cancel', '\App\Http\Controllers\API\UserController@cancelSubscription');
    Route::get('/user/status', '\App\Http\Controllers\API\UserController@getStatus');

    // Integrations
    Route::group(['prefix' => 'int'], function () {
        Route::group(['prefix' => 'toggl'], function () {
            Route::get('/import-hours', '\App\Http\Controllers\Integrations\Toggl\TogglController@getHours')->name('integrations.toggl.hours');
            Route::get('/projects/{force?}', '\App\Http\Controllers\Integrations\Toggl\TogglController@getProjects')->name('integrations.toggl.projects');
            Route::post('/hours', '\App\Http\Controllers\Integrations\Toggl\TogglController@getProjectHours')->name('integrations.toggl.project.hours');
        });
    });
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', '\App\Http\Controllers\Auth\LoginController@login');
    Route::post('register', '\App\Http\Controllers\Auth\RegisterController@register');

    Route::post('password/email', '\App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', '\App\Http\Controllers\Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', '\App\Http\Controllers\Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', '\App\Http\Controllers\Auth\VerificationController@resend');

    Route::post('oauth/{driver}', '\App\Http\Controllers\Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', '\App\Http\Controllers\Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});
